<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankMember extends Model {
    
    protected $table    = 'bank_member';
    
    public $timestamps   = false;
    
    public function member(){
        return $this->belongsTo(Member::class, 'id_member', 'id');
    }
}
