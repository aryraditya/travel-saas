<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable {

    use Notifiable;

    protected $table = 'customer';
    
    public $timestamps   = false;

    protected $fillable = ['nama','no_telp','email','password','alamat','tipe_id','no_id','status'];
}
