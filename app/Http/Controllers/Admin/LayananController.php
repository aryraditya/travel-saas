<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Layanan;

class LayananController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = Layanan::when($search, function($query) use ($search) {
                    build_like_query($query, 'nama', $search);
                    build_or_like_query($query, 'keterangan', $search);
                })
                ->paginate(10);
        
        return view('admin.layanan.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $model          = new Layanan();
        
        return view('admin.layanan.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator      = \Validator::make($request->all(),[
            'nama'      => 'required',
            'durasi'    => 'required|numeric',
            'harga'     => 'required',
        ], [
            'nama.required'     => 'Nama harus diisi',
            'durasi.required'   => 'Durasi harus diisi',
            'harga.required'    => 'Harga harus diisi',
            'harga.numeric'     => 'Harga harus bersifat numeric',
            'durasi.numeric'     => 'Harga harus bersifat numeric',
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $model              = new Layanan();
        $model->nama        = $request->nama;
        $model->durasi      = $request->durasi;
        $model->harga       = (double)strtr($request->harga, ['.'=>'',','=>'.']);
        $model->keterangan  = $request->keterangan;
        
        $model->save();
        
        \Session::flash('success','Data layanan berhasil ditambahkan');
        return redirect()->route('admin.layanan.edit', ['id' =>$model->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $model      = Layanan::findOrFail($id);
        
        return view('admin.layanan.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator      = \Validator::make($request->all(),[
            'nama'      => 'required',
            'durasi'    => 'required|numeric',
            'harga'     => 'required',
        ], [
            'nama.required'     => 'Nama harus diisi',
            'durasi.required'   => 'Durasi harus diisi',
            'harga.required'    => 'Harga harus diisi',
            'harga.numeric'     => 'Harga harus bersifat numeric',
            'durasi.numeric'     => 'Harga harus bersifat numeric',
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $model              = Layanan::findOrFail($id);
        $model->nama        = $request->nama;
        $model->durasi      = $request->durasi;
        $model->harga       = (double)strtr($request->harga, ['.'=>'',','=>'.']);
        $model->keterangan  = $request->keterangan;
        
        $model->save();
        
        \Session::flash('success','Data layanan berhasil disimpan');
        return redirect()->route('admin.layanan.edit', ['id' =>$model->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
