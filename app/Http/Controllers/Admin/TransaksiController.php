<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\LayananMember;

class TransaksiController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = LayananMember::orderBy('id','desc')
                ->when($search, function($query) use ($search) {
                    build_like_query($query, 'id', trim(strtolower($search),'trx-'));
                })
                ->orWhereHas('member', function($query) use ($search){
                    build_like_query($query, 'nama_usaha', $search);
                })
                ->paginate(10);

        return view('admin.transaksi.index', compact('models'));
    }

    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $model                  = LayananMember::findOrFail($id);
        $model->status          = 10;
        
        $member                 = $model->member;
        $member->id_layanan     = $model->id_layanan;
        $member->tgl_aktif      = date('Y-m-d H:i:s');
        
        $tgl                    = date('Y-m-d');
        if($member->tgl_tenggang > $tgl){
            $tgl                = $member->tgl_tenggang;
        }
        
        $member->tgl_tenggang   = date('Y-m-d', strtotime('+'.$model->layanan->durasi.'month', strtotime($tgl)));
        $member->status         = 1;
        $member->save();

        $model->save();

        \Session::flash('success', 'Data transaksi berhasil disimpan');
        return redirect()->route('admin.transaksi.index');
    }

}
