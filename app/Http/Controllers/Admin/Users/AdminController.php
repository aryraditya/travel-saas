<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;

class AdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = Admin::when($search, function($query) use ($search) {
                    build_like_query($query, 'nama', $search);
                    build_or_like_query($query, 'email', $search);
                })
                ->paginate(10);

        return view('admin.users.admin.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $model      = new Admin();
        
        return view('admin.users.admin.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $model          = new Admin();
        
        $validator      = \Validator::make($request->all(),[
            'nama'      => 'required',
            'email'     => 'required|email|unique:admin,email',
            'password'  => 'required',
            'status'    => 'required'
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        
        $model->nama    = $request->nama;
        $model->email   = $request->email;
        $model->password= bcrypt($request->password);
        $model->status  = $request->status;
        
        $model->save();
        
        \Session::flash('success', 'Berhasil menambahkan admin baru');
        return redirect()->route('admin.users.admin.edit', ['id' => $model->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $model          = Admin::findOrFail($id);
        
        return view('admin.users.admin.form', compact('model'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $model          = Admin::findOrFail($id);
        
        $validator      = \Validator::make($request->all(),[
            'nama'      => 'required',
            'email'     => 'required|email',
            'status'    => 'required'
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        
        $model->nama    = $request->nama;
        $model->email   = $request->email;
        
        if($request->password)
            $model->password= bcrypt($request->password);
        
        $model->status  = $request->status;
        
        $model->save();
        
        \Session::flash('success', 'Berhasil merubah data admin');
        return redirect()->route('admin.users.admin.edit', ['id' => $model->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
