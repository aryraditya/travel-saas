<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Customer;

class CustomerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = Customer::when($search, function($query) use ($search) {
                    build_like_query($query, 'nama', $search);
                    build_or_like_query($query, 'email', $search);
                    build_or_like_query($query, 'no_telp', $search);
                    build_or_like_query($query, 'alamat', $search);
                })
                ->paginate(10);

        return view('admin.users.customer.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $model = new Customer();

        return view('admin.users.customer.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $model = new Customer();

        $validator = \Validator::make($request->all(), [
                    'nama' => 'required',
                    'email' => 'required|email',
                    'password' => 'required',
                    'status' => 'required',
                    'no_telp' => 'required',
                    'alamat' => 'required',
                    'no_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }

        $model->nama = $request->nama;
        $model->email = $request->email;
        $model->password = bcrypt($request->password);
        $model->status = $request->status;
        $model->no_telp = $request->no_telp;
        $model->alamat  = $request->alamat;
        $model->tipe_id = $request->tipe_id;
        $model->no_id   = $request->no_id;
        $model->status  = $request->status;

        $model->save();

        \Session::flash('success', 'Berhasil menambahkan customer baru');
        return redirect()->route('admin.users.customer.edit', ['id' => $model->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $model = Customer::findOrFail($id);

        return view('admin.users.customer.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $model = Customer::findOrFail($id);

        $validator = \Validator::make($request->all(), [
                    'nama' => 'required',
                    'email' => 'required|email',
                    'status' => 'required',
                    'no_telp' => 'required',
                    'alamat' => 'required',
                    'no_id' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }

        $model->nama = $request->nama;
        $model->email = $request->email;
        $model->status = $request->status;
        $model->no_telp = $request->no_telp;
        $model->alamat  = $request->alamat;
        $model->tipe_id = $request->tipe_id;
        $model->no_id   = $request->no_id;
        $model->status  = $request->status;

        if ($request->password)
            $model->password = bcrypt($request->password);

        $model->status = $request->status;

        $model->save();

        \Session::flash('success', 'Berhasil merubah data customer');
        return redirect()->route('admin.users.customer.edit', ['id' => $model->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
