<?php

namespace App\Http\Controllers\Admin\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Member;

class MemberController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = Member::when($search, function($query) use ($search) {
                    build_like_query($query, 'nama_pemilik', $search);
                    build_or_like_query($query, 'nama_usaha', $search);
                    build_or_like_query($query, 'email', $search);
                    build_or_like_query($query, 'alamat', $search);
                })
                ->paginate(10);

        return view('admin.users.member.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $model      = new Member();
        
        return view('admin.users.member.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $model          = new Member();
        
        $validator      = \Validator::make($request->all(),[
            'nama_usaha'        => 'required',
            'nama_pemilik'      => 'required',
            'no_telp'           => 'required|numeric',
            'kode_pos'          => 'numeric',
            'alamat'            => 'required',
            'tgl_tenggang'      => 'required',
            'id_layanan'        => 'required',
            'email'             => 'required|email|unique:member,email',
            'password'          => 'required',
            'status'            => 'required'
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        
        $model->nama_usaha      = $request->nama_usaha;
        $model->nama_pemilik    = $request->nama_pemilik;
        $model->no_telp         = $request->no_telp;
        $model->kode_pos        = $request->kode_pos;
        $model->alamat          = $request->alamat;
        $model->tgl_tenggang    = $request->tgl_tenggang;
        $model->id_layanan      = $request->id_layanan;
        $model->email           = $request->email;
        $model->password        = bcrypt($request->password);
        $model->status          = $request->status;
        $model->tgl_aktif       = date('Y-m-d H:i:s');
        
        $model->save();
        
        \Session::flash('success', 'Berhasil menambahkan member baru');
        return redirect()->route('admin.users.member.edit', ['id' => $model->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $model          = Member::findOrFail($id);
        
        return view('admin.users.member.form', compact('model'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $model          = Member::findOrFail($id);
        
        $validator      = \Validator::make($request->all(),[
            'nama_usaha'        => 'required',
            'nama_pemilik'      => 'required',
            'no_telp'           => 'required|numeric',
            'kode_pos'          => 'numeric',
            'alamat'            => 'required',
            'tgl_tenggang'      => 'required',
            'id_layanan'        => 'required',
            'email'             => 'required|email',
            'status'            => 'required'
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        
        $model->nama_usaha      = $request->nama_usaha;
        $model->nama_pemilik    = $request->nama_pemilik;
        $model->no_telp         = $request->no_telp;
        $model->kode_pos        = $request->kode_pos;
        $model->alamat          = $request->alamat;
        $model->tgl_tenggang    = $request->tgl_tenggang;
        $model->id_layanan      = $request->id_layanan;
        $model->email           = $request->email;
        $model->status          = $request->status;
        
        if($request->password)
            $model->password        = bcrypt($request->password);
        
        $model->save();
        
        \Session::flash('success', 'Berhasil menyimpan perubahan data member');
        return redirect()->route('admin.users.member.edit', ['id' => $model->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
