<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Wilayah;

class WilayahController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = Wilayah::orderBy('nama','asc')
                ->when($search, function($query) use ($search) {
                    build_like_query($query, 'nama', $search);
                })
                ->paginate(10);

        return view('admin.wilayah.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $model = new Wilayah();

        return view('admin.wilayah.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = \Validator::make($request->all(), [
                    'nama' => 'required',
                    ], [
                    'nama.required' => 'Nama wilayah harus diisi',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $model = new Wilayah();
        $model->nama = $request->nama;

        $model->save();

        \Session::flash('success', 'Data wilayah berhasil ditambahkan');
        return redirect()->route('admin.wilayah.edit', ['id' => $model->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $model = Wilayah::findOrFail($id);

        return view('admin.wilayah.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $validator = \Validator::make($request->all(), [
                    'nama' => 'required',
                    ], [
                    'nama.required' => 'Nama wilayah harus diisi',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $model = Wilayah::findOrFail($id);
        $model->nama = $request->nama;

        $model->save();

        \Session::flash('success', 'Data wilayah berhasil disimpan');
        return redirect()->route('admin.wilayah.edit', ['id' => $model->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
