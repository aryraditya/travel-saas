<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller {
    
    public function edit(){
        $model      = \Auth::guard('member')->user();
        
        return view('member.account.edit', compact('model'));
    }
    
    public function update(Request $request){
        $model      = \Auth::guard('member')->user();
        
        $validator      = \Validator::make($request->all(),[
            'nama_usaha'        => 'required',
            'nama_pemilik'      => 'required',
            'no_telp'           => 'required|numeric',
            'kode_pos'          => 'numeric',
            'alamat'            => 'required',
            'email'             => 'required|email',
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        
        $model->nama_usaha      = $request->nama_usaha;
        $model->nama_pemilik    = $request->nama_pemilik;
        $model->no_telp         = $request->no_telp;
        $model->kode_pos        = $request->kode_pos;
        $model->alamat          = $request->alamat;
        $model->email           = $request->email;
        
        if($request->password)
            $model->password        = bcrypt($request->password);
        
        $model->save();
        
        \Session::flash('success', 'Berhasil menyimpan perubahan');
        return redirect()->back();
    }
    
    
    public function upgrade(){
        
        return view('member.account.upgrade');
    }
    
    public function storeUpgrade(Request $request){
        $model              = new \App\LayananMember();
        $layanan            = \App\Layanan::findOrFail($request->id_layanan);
        
        $model->id_member   = \Auth::guard('member')->id();
        $model->id_layanan  = $request->id_layanan;
        $model->status      = 0;
        $model->total       = $layanan->harga;
        
        $model->save();
        
        return redirect()->route('member.upgrade.konfirmasi', ['id' => $model->id]);
    }
    
    public function pembayaran(Request $request, $id){
        $model      = \App\LayananMember::findOrFail($id);
        
        return view('member.auth.pembayaran', compact('model'));
    }
}
