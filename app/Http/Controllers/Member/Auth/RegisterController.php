<?php

namespace App\Http\Controllers\Member\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Member;

class RegisterController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest.member')->except(['pembayaran','konfirmasi']);
    }
    
    public function showForm(){
        $model      = new Member();
        
        return view('member.auth.register', compact('model'));
    }
    
    public function register(Request $request){
        $model          = new Member();
        
        $validator      = \Validator::make($request->all(),[
            'nama_usaha'        => 'required',
            'nama_pemilik'      => 'required',
            'no_telp'           => 'required|numeric',
            'kode_pos'          => 'required|numeric',
            'alamat'            => 'required',
            'email'             => 'required|email|unique:member,email',
            'password'          => 'required',
            'id_layanan'        => 'required',
        ], [
            'nama_usaha.required'       => 'Nama usaha harus diisi',
            'nama_pemilik.required'     => 'Nama pemilik harus diisi',
            'no_telp.required'          => 'No telp harus diisi',
            'kode_pos.required'         => 'Kode pos harus diisi',
            'alamat.required'           => 'Alamat harus diisi',
            'email.required'            => 'Email usaha harus diisi',
            'password.required'         => 'Password usaha harus diisi',
            'email.email'               => 'Email tidak valid',
            'id_layanan.required'       => 'Pilih layanan',
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        
        $model->nama_usaha      = $request->nama_usaha;
        $model->nama_pemilik    = $request->nama_pemilik;
        $model->no_telp         = $request->no_telp;
        $model->kode_pos        = $request->kode_pos;
        $model->alamat          = $request->alamat;
        $model->id_layanan      = $request->id_layanan;
        $model->email           = $request->email;
        $model->password        = bcrypt($request->password);
        $model->status          = 0;
        
        if($model->save()){
            $transaksi              = new \App\LayananMember();
            $transaksi->id_member   = $model->id;
            $transaksi->id_layanan  = $model->id_layanan;
            $transaksi->total       = $model->layanan->harga;
            $transaksi->status      = 0;
            $transaksi->save();
        }
        
        return redirect()->route('member.auth.pembayaran', ['id' => $transaksi->id]);
    }
    
    public function pembayaran(Request $request, $id){
        $model      = \App\LayananMember::findOrFail($id);
        
        return view('member.auth.pembayaran', compact('model'));
    }
    
    public function konfirmasi(Request $request, $id){
        $model      = \App\LayananMember::findOrFail($id);
        
        $validator  = \Validator::make($request->all(), [
            'from_bank'     => 'required',
            'from_no'       => 'required',
            'to_bank'       => 'required'
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors)->withInput();
        }
        
        $model->tgl_bayar   = date('Y-m-d H:i:s');
        $model->konfirmasi_ke_bank  = $request->to_bank;
        $model->konfirmasi_dari_bank= $request->from_bank.'-'.$request->from_no;
        $model->status      = 5;
        
        $model->save();
        \Session::flash('success', 'Konfirmasi pembayaran telah disubmit, Silahkan tunggu konfirmasi dari admin mengenai pembayaran Anda.');
        
        return redirect()->back();
    }
}
