<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\BankMember as Bank;

class BankController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = Bank::orderBy('nama_bank', 'asc')
                ->where('id_member','=', \Auth::guard('member')->id())
                ->where(function($query) use ($search){
                    build_like_query($query, 'nama_bank', $search);
                    build_or_like_query($query, 'no_rek', $search);
                })
                ->paginate(10);

        return view('member.bank.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $model      = new Bank();

        return view('member.bank.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator              = \Validator::make($request->all(), [
            'nama'              => 'required',
            'no'                => 'required|numeric',
            'kcp'               => 'required',
            'atas_nama'               => 'required',
        ], [
            'nama.required'     => 'Nama bank harus diisi',
            'no.required'       => 'No rekening harus diisi',
            'kcp.required'      => 'KCP tour harus diisi',
            'atas_nama.required'=> 'Nama pemilik rekening harus diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $model              = new Bank();
        $model->nama_bank   = strtoupper($request->nama);
        $model->no_rek      = $request->no;
        $model->kcp         = $request->kcp;
        $model->atas_nama   = $request->atas_nama;
        $model->id_member   = \Auth::guard('member')->id();

        $model->save();

        \Session::flash('success', 'Data bank berhasil ditambahkan');
        return redirect()->route('member.bank.edit', ['id' => $model->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $model = Bank::findOrFail($id);

        return view('member.bank.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $model                  = Bank::findOrFail($id);
        
        $validator              = \Validator::make($request->all(), [
            'nama'              => 'required',
            'no'                => 'required|numeric',
            'kcp'               => 'required',
            'atas_nama'               => 'required',
        ], [
            'nama.required'     => 'Nama bank harus diisi',
            'no.required'       => 'No rekening harus diisi',
            'kcp.required'      => 'KCP tour harus diisi',
            'atas_nama.required'=> 'Nama pemilik rekening harus diisi'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $model->nama_bank   = strtoupper($request->nama);
        $model->no_rek      = $request->no;
        $model->kcp         = $request->kcp;
        $model->atas_nama   = $request->atas_nama;
        $model->id_member   = \Auth::guard('member')->id();

        $model->save();

        \Session::flash('success', 'Data bank berhasil disimpan');
        return redirect()->route('member.bank.edit', ['id' => $model->id]);
    }
    
    
    public function destroy($id){
        $model              = Bank::findOrFail($id);
        
        $model->delete();
        
        return redirect()->route('member.bank.index');
    }

}
