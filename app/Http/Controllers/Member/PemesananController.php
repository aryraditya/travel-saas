<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Pesanan;

class PemesananController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');
        $from   = $request->get('from');
        $to     = $request->get('to');
        $status = $request->get('status');

        $models = Pesanan::orderBy('id', 'desc')
                ->whereHas('tour', function($query) use ($search) {
                    $query->where('id_member', '=', \Auth::guard('member')->id());
                })
                ->where(function($query) use ($search){
                    $query->when($search, function($query) use ($search) {
                        build_like_query($query, 'id', trim(strtolower($search),'rsv-'));
                    })
                    ->orWhereHas('customer', function($query) use ($search) {
                        build_like_query($query, 'nama', $search);
                        build_or_like_query($query, 'email', $search);
                    })
                    ->orWhereHas('tour', function($query) use ($search) {
                        build_like_query($query, 'nama', $search);
                    });
                })
                ->when($from, function($query) use ($from){
                    $query->where('created_at', '>=', $from);
                })
                ->when($to, function($query) use ($to){
                    $query->where('created_at', '<=', $to);
                })
                ->where(function($query) use ($status){
                    if(!in_array($status,[null,-10])){
                        $query->where('status','=',$status);
                    }
                })
                ->paginate(10);
                

        return view('member.pemesanan.index', compact('models'));
    }
    
    
    public function prints(Request $request){
        $search = $request->get('search');
        $from   = $request->get('from', date('Y-m-01'));
        $to     = $request->get('to', date('Y-m-t'));
        $status = $request->get('status');

        $models = Pesanan::orderBy('id', 'desc')
                ->whereHas('tour', function($query) use ($search) {
                    $query->where('id_member', '=', \Auth::guard('member')->id());
                })
                ->where(function($query) use ($search){
                    $query->when($search, function($query) use ($search) {
                        build_like_query($query, 'id', trim(strtolower($search),'rsv-'));
                    })
                    ->orWhereHas('customer', function($query) use ($search) {
                        build_like_query($query, 'nama', $search);
                        build_or_like_query($query, 'email', $search);
                    })
                    ->orWhereHas('tour', function($query) use ($search) {
                        build_like_query($query, 'nama', $search);
                    });
                })
                ->when($from, function($query) use ($from){
                    $query->where('created_at', '>=', $from);
                })
                ->when($to, function($query) use ($to){
                    $query->where('created_at', '<=', $to);
                })
                ->where(function($query) use ($status){
                    if(!in_array($status,[null,-10])){
                        $query->where('status','=',$status);
                    }
                })
                ->get();
        
                
        $pdf = \PDF::loadView('member.pemesanan.laporan', compact('models', 'from','to'));
                
	return $pdf->stream('document.pdf');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $model              = Pesanan::findOrFail($id);
        
        return view('member.pemesanan.show', compact('model'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $model                  = Pesanan::findOrFail($id);
        
        if($request->input('action') == 'cancel'){
            $model->status      = -1;
        }else {
            $model->status      = 10;
        }
        $model->save();
        
        return redirect()->route('member.pemesanan.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
