<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Tour;

class TourController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $search = $request->get('search');

        $models = Tour::orderBy('id', 'asc')
                ->where('id_member','=', \Auth::guard('member')->id())
                ->where(function($query) use ($search){
                    $query->when($search, function($query) use ($search) {
                            build_like_query($query, 'nama', $search);
                        })
                        ->orWhereHas('getWilayah', function($query) use ($search) {
                            build_like_query($query, 'nama', $search);
                        });
                })
                ->paginate(10);

        return view('member.tour.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $model      = new Tour();

        return view('member.tour.form', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator              = \Validator::make($request->all(), [
            'nama'              => 'required',
            'wilayah'           => 'required',
            'harga'             => 'required',
            'deskripsi'         => 'required',
            'min_pax'           => 'numeric|min:1',
            'gambar_1'          => 'required | image:jpeg,jpg,png | max:5000',
            'gambar_2'          => 'nullable | image:jpeg,jpg,png | max:5000',
            'gambar_3'          => 'nullable | image:jpeg,jpg,png | max:5000',
        ], [
            'nama.required'     => 'Nama tour harus diisi',
            'wilayah.required'  => 'Pilih wilayah tour',
            'harga.required'    => 'Harga tour harus diisi',
            'deskripsi.required'=> 'Deskripsi harus diisi',
            'gambar_1.required' => 'Gambar 1 harus diisi',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }

        $model              = new Tour();
        $model->nama        = $request->nama;
        $model->wilayah     = $request->wilayah;
        $model->harga       = (double)strtr($request->harga, ['.'=>'',','=>'.']);
        $model->deskripsi   = $request->deskripsi;
        $model->status      = $request->status;
        $model->min_pax     = $request->min_pax;
        $model->max_pax     = $request->max_pax;
        $model->id_member   = \Auth::guard('member')->id();
        
        $model->gambar_1    = $model->uploadImage($request->file('gambar_1'));
        $model->gambar_2    = $model->uploadImage($request->file('gambar_2'));
        $model->gambar_3    = $model->uploadImage($request->file('gambar_3'));

        $model->save();

        \Session::flash('success', 'Data tour berhasil ditambahkan');
        return redirect()->route('member.tour.edit', ['id' => $model->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $model = Tour::findOrFail($id);

        return view('member.tour.form', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $model                  = Tour::findOrFail($id);
        
        $validator              = \Validator::make($request->all(), [
            'nama'              => 'required',
            'wilayah'           => 'required',
            'harga'             => 'required',
            'deskripsi'         => 'required',
            'gambar_1'          => 'nullable | image:jpeg,jpg,png | max:5000',
            'gambar_2'          => 'nullable | image:jpeg,jpg,png | max:5000',
            'gambar_3'          => 'nullable | image:jpeg,jpg,png | max:5000',
        ], [
            'nama.required'     => 'Nama tour harus diisi',
            'wilayah.required'  => 'Pilih wilayah tour',
            'harga.required'    => 'Harga tour harus diisi',
            'deskripsi.required'=> 'Deskripsi harus diisi',
        ]);
//        dd($request);

        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        $model->nama        = $request->nama;
        $model->wilayah     = $request->wilayah;
        $model->harga       = (double)strtr($request->harga, ['.'=>'',','=>'.']);
        $model->deskripsi   = $request->deskripsi;
        $model->status      = $request->status;
        $model->min_pax     = $request->min_pax;
        $model->max_pax     = $request->max_pax;
        $model->id_member   = \Auth::guard('member')->id();
        
        $model->gambar_1    = $model->uploadImage($request->file('gambar_1'), $model->gambar_1);
        $model->gambar_2    = $model->uploadImage($request->file('gambar_2'), $model->gambar_2);
        $model->gambar_3    = $model->uploadImage($request->file('gambar_3'), $model->gambar_3);

        $model->save();

        \Session::flash('success', 'Data tour berhasil disimpan');
        return redirect()->route('member.tour.edit', ['id' => $model->id]);
    }

}
