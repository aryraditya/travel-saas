<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;

class HomeController extends Controller{
    
    public function index(){
        $wilayah        = \App\Wilayah::orderBy('nama')->get();
        
        $tours          = \App\Tour::selectRaw('paket_tour.*, pesanan.*')
                            ->leftJoin(\DB::raw('(SELECT count(id) as counts, id_tour FROM pesanan GROUP BY id_tour) as pesanan'),'paket_tour.id', 'pesanan.id_tour')
                            ->orderBy('counts', 'desc')
                            ->where(['status' => 1])
                            ->paginate(6);
        
        return view('site.index', compact('wilayah', 'tours'));
    }
}