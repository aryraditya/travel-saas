<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pesanan;

class ProfileController extends Controller {
    
    public function pesanan(){
        $models     = Pesanan::where([
                    'id_cust'       => \Auth::id()
                ])
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        
        return view('site.profile.pesanan', compact('models'));
    }
    
    public function setting(){
        $model      = \Auth::user();
        
        return view('site.profile.setting', compact('model'));
    }
    
    public function simpan(Request $request){
        $model      = \Auth::user();
        
        $validator  = \Validator::make($request->all(), [
                    'nama' => 'required|string|max:255',
                    'email' => 'required|string|email|max:255',
                    'no_telp' => 'required',
                    'alamat' => 'required',
                    'no_id' => 'required',
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        
        $model->nama        = $request->nama;
        $model->no_telp     = $request->no_telp;
        $model->email       = $request->email;
        $model->alamat      = $request->alamat;
        $model->tipe_id     = $request->tipe_id;
        $model->no_id       = $request->no_id;
        
        if($request->password){
            $model->password = bcrypt($request->password);
        }
        
        $model->save();
        
        return redirect()->back();
    }
}
