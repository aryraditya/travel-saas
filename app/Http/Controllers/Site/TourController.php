<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tour;
use App\Wilayah;
use App\Pesanan;

class TourController extends Controller {
    
    public function search(Request $request){
        
        $wilayahs       = Wilayah::orderBy('nama')->get();
        $search         = $request->get('q');
        $wilayah        = $request->get('w');
        
        $models         = Tour::selectRaw('paket_tour.*, pesanan.*')
                            ->leftJoin(\DB::raw('(SELECT count(id) as counts, id_tour FROM pesanan GROUP BY id_tour) as pesanan'),'paket_tour.id', 'pesanan.id_tour')
                            ->where(['status' => 1])
                            ->when($search, function($query) use ($search) {
                                build_like_query($query, 'nama', $search);
                            })
                            ->whereHas('getWilayah', function($query) use ($wilayah) {
                                if($wilayah != 0)
                                    $query->where('id','=',$wilayah);
                            });
        
        switch($request->get('s')){
            case 0 :
                $models->orderBy('counts', 'desc');
                break;
            case 1 :
                $models->orderBy('nama','asc');
                break;
            case 2 :
                $models->orderBy('nama','desc');
                break;
            case 3 :
                $models->orderBy('harga','asc');
                break;
            case 4 :
                $models->orderBy('harga','desc');
                break;
        }
        
        $models         = $models->paginate(10);
        
        return view('site.tour.search', compact('models','wilayahs'));
    }
    
    
    public function detail(Request $request, $id){
        $model          = Tour::findOrFail($id);
        
        return view('site.tour.detail', compact('model'));
    }
    
    public function pesan(Request $request){
        $model          = Tour::findOrFail($request->tour_id);
        $current_url    = route(\Route::currentRouteName(), $request->all());
        
        if(\Auth::guest()){
            return redirect()->route('login',['url'=>$current_url]);
        }
        $user                   = \Auth::user();
        $model->cust_nama       = $user->nama;
        $model->cust_email      = $user->email;
        $model->cust_no_telp    = $user->no_telp;
        $model->cust_alamat     = $user->alamat;
        $model->cust_tipe_id    = $user->tipe_id;
        $model->cust_no_id      = $user->no_id;
        
        return view('site.tour.pesan', compact('model'));
    }
    
    public function proses(Request $request){
        $model                  = new Pesanan();
        $tour                   = Tour::findOrFail($request->id_tour);
        
        $validator              = \Validator::make([
            'nama'              => 'required',
            'email'             => 'email|required',
            'no_telp'           => 'required',
            'alamat'            => 'required',
            'no_id'             => 'required',
        ],[
            'nama.required'     => 'Nama harus diisi',
            'no_telp.required'  => 'Nama harus diisi',
            'email.required'    => 'Nama harus diisi',
            'email.email'       => 'Format email salah',
            'alamat.required'   => 'Nama harus diisi',
            'no_id.required'    => 'Nama harus diisi',
        ]);
        
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors())->withInput($request->all());
        }
        
        $model->id_cust         = \Auth::id();
        $model->cust_nama       = $request->nama;
        $model->cust_email      = $request->email;
        $model->cust_no_telp    = $request->no_telp;
        $model->cust_alamat     = $request->alamat;
        $model->cust_tipe_id    = $request->tipe_id;
        $model->cust_no_id      = $request->no_id;
        
        $model->id_tour         = $request->id_tour;
        $model->tgl_tour        = $request->tanggal_tour;
        $model->total_pax       = $request->total_pax;
        $model->metode_bayar    = $request->metode_bayar;
        
        $model->total           = $tour->harga * $model->total_pax;
        $model->status          = 0;
        
        $model->save();
        
        return redirect()->route('bayar', ['id' => $model->id]);
    }
    
    public function konfirmasi(Request $request, $id){
        $model                  = Pesanan::findOrFail($id);
        $batas                  = date('Y/m/d H:i:s', strtotime('+12hour', strtotime($model->created_at)));
        $now                    = date('Y-m-d H:i:s');
        
        if($now > date('Y-m-d H:i:s', strtotime($batas))){
            $model->status = -1;
            $model->save();
        }
        
        return view('site.tour.bayar', compact('model','batas'));
    }
    
    public function bayar(Request $request, $id){
        $model                  = Pesanan::findOrFail($id);
        
        $model->tgl_bayar       = date('Y-m-d');
        $model->konfirmasi_ke_bank = $request->to_bank;
        $model->konfirmasi_dari_bank = $request->from_bank . ' - ' . $request->from_no;
        
        $model->status          = 5;
        
        $model->save();
        
        return redirect()->route('bayar', ['id' => $model->id]);
    }
}
