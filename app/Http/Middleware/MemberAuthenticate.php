<?php

namespace App\Http\Middleware;

use Closure;

class MemberAuthenticate {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $auth = \Auth::guard('member');
        if (!$auth->check()) {
            return redirect()->route('member.auth.login');
        }
        
        if($auth->user()->tgl_tenggang == null){
            $langganan  = \App\LayananMember::where(['id_member' => $auth->user()->id])->orderBy('created_at', 'desc')->first();
            return redirect()->route('member.auth.pembayaran', ['id' => $langganan->id]);
        }
        
        if($auth->user()->tgl_tenggang < date('Y-m-d')){
            \Session::flash('inactive', 'Akun Anda telah memasuki masa tenggang');
//            dd(\Route::currentRouteName());
            if(!in_array(\Route::currentRouteName(), ['member.dashboard', 'member.upgrade', 'member.auth.pembayaran', 'member.upgrade.konfirmasi'])){
                return redirect()->route('member.dashboard');
            }
        }
        return $next($request);
    }

}
