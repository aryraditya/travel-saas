<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RedirectIfAdminAuthenticated {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $auth = Auth::guard('admin');
        if ($auth->check()) {
            return redirect()->route('admin.dashboard');
        }
        return $next($request);
    }

}
