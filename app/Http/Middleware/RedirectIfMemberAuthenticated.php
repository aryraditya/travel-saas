<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfMemberAuthenticated {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $auth = \Auth::guard('member');
        if ($auth->check()) {
            return redirect()->route('member.dashboard');
        }
        return $next($request);
    }

}
