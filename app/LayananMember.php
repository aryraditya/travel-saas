<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LayananMember extends Model {

    //
    protected $table = 'langganan_member';

    public function member() {
        return $this->belongsTo(Member::class, 'id_member', 'id');
    }

    public function layanan() {
        return $this->belongsTo(Layanan::class, 'id_layanan', 'id');
    }

    public static function totalTransaksi($status = 10, $bulan = null, $tahun = null) {
        return self::when($bulan, function($query) use ($bulan) {
                            $query->whereMonth('created_at', '=', $bulan);
                        })->when($tahun, function($query) use ($tahun) {
                            $query->whereYear('created_at', '=', $tahun);
                        })
                        ->where('status', '=', $status)
                        ->count();
    }

    public static function totalPendapatan($status = 10, $bulan = null, $tahun = null) {
        return self::when($bulan, function($query) use ($bulan) {
                            $query->whereMonth('created_at', '=', $bulan);
                        })->when($tahun, function($query) use ($tahun) {
                            $query->whereYear('created_at', '=', $tahun);
                        })
                        ->where('status', '=', $status)
                        ->sum('total');
    }
    
    public static function arrayTotalPendapatan($tahun){
        $results = [];
        
        for($i = 1; $i <= 12; $i++){
            $results[] = [$i, (int) self::totalPendapatan(10, $i, $tahun)];
        }
        
        return $results;
    }

}
