<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Member extends Authenticatable {
    
    use Notifiable;
    
    protected $table = 'member';
    
    public function layanan(){
        return $this->belongsTo(Layanan::class, 'id_layanan', 'id');
    }
    
    public function bank(){
        return $this->hasMany(BankMember::class, 'id_member','id');
    }
}
