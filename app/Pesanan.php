<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pesanan extends Model {
    protected $table = 'pesanan';
    
    public function customer(){
        return $this->belongsTo(Customer::class, 'id_cust','id');
    }
    
    public function tour(){
        return $this->belongsTo(Tour::class,'id_tour','id');
    }
    
    public static function byMember(){
        return self::whereHas('tour', function($query){
            $query->where('id_member', '=', \Auth::guard('member')->id());
        });
    }
    public static function totalPesanan($member, $bulan = null, $tahun = null){
        return self::whereHas('tour', function($query) use ($member){
            $query->where('id_member', '=', $member);
        })->when($bulan, function($query) use ($bulan){
            $query->whereMonth('created_at', '=', $bulan);
        })->when($tahun, function($query) use ($tahun){
            $query->whereYear('created_at', '=', $tahun);
        })
        ->where('status','=','10')
        ->count();
    }
    
    public static function totalPendapatan($member, $bulan = null, $tahun = null){
        return self::whereHas('tour', function($query) use ($member) {
                    $query->where('id_member', '=', $member);
                })->when($bulan, function($query) use ($bulan) {
                    $query->whereMonth('created_at', '=', $bulan);
                })->when($tahun, function($query) use ($tahun) {
                    $query->whereYear('created_at', '=', $tahun);
                })
                ->where('status','=','10')
                ->sum('total');
    }
    
    public static function arrayTotalPesanan($member, $tahun){
        $results = [];
        
        for($i = 1; $i <= 12; $i++){
            $results[] = [$i, self::totalPesanan($member, $i, $tahun)];
        }
        
        return $results;
    }
    
    public static function arrayTotalPendapatan($member, $tahun){
        $results = [];
        
        for($i = 1; $i <= 12; $i++){
            $results[] = [$i, self::totalPendapatan($member, $i, $tahun)];
        }
        
        return $results;
    }
}
