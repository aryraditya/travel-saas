<?php

if (!function_exists('escape_like')) {

    /**
     * @param $string
     * @return mixed
     */
    function escape_like($string) {
        $search = array('%', '_');
        $replace = array('\%', '\_');
        return str_replace($search, $replace, $string);
    }

}

if (!function_exists('build_like_query')) {

    /**
     * Build like query
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $column
     * @param string $keywords
     * @param  string  $boolean
     * @return \Illuminate\Database\Eloquent\Builder
     */
    function build_like_query($query, $column, $keywords, $boolean = 'and') {
        $keywords   = array_filter(explode(' ', $keywords));
        $boolean    = strtolower($boolean);

        $query->where(function($q) use ($column, $keywords) {
            foreach ($keywords as $keyword) {
                $q->where($column, 'LIKE', '%' . escape_like($keyword) . '%');
            }
        }, null, null, $boolean);

        return $query;
    }

}


if (!function_exists('build_or_like_query')) {

    /**
     * Build like query
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string $column
     * @param string $keywords
     * @return \Illuminate\Database\Eloquent\Builder
     */
    function build_or_like_query($query, $column, $keywords) {
        return build_like_query($query, $column, $keywords, 'or');
    }

}


if (!function_exists('short_description')) {
    
    function short_description($text, $limit = 30) {
        $text       = strip_tags($text);
        $exploded   = array_filter(explode(' ', $text));
        $no         = 0;
        $store      = [];
        
        while($no <= $limit){
            $store[] = $exploded[$no];
            
            $no++;
        }
        
        
        return implode(' ',$store);
    }

}
