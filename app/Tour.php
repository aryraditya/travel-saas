<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use File;

class Tour extends Model {

    protected $table = 'paket_tour';
    
    public function getWilayah(){
        return $this->belongsTo(Wilayah::class, 'wilayah','id');
    }
    
    public function pesanan(){
        return $this->hasMany(Pesanan::class, 'id_tour', 'id');
    }
    
    public function owner(){
        return $this->belongsTo(Member::class, 'id_member','id');
    }
    
    public function getImage($all = false){
        $images     = array_filter([
            $this->gambar_1,
            $this->gambar_2,
            $this->gambar_3
        ]);
        
        if($all)
            return $images;
        
        return array_first($images);
    }

    public function getImagePath($image = null, $realPath = false) {
        $path = 'uploads/' . $this->id_member . '/';

        if ($realPath)
            return public_path($path);

        return url($path.$image);
    }

    public function uploadImage($image, $oldImage = null) {
        if (!empty($image)) {
            $fileName       = time() . rand();
            $name           = str_slug($this->nama) . '_' . $fileName . '.' . $image->getClientOriginalExtension();

            $image->move($this->getImagePath(null, true), $name);
            if (File::exists($this->getImagePath(null, true) . $oldImage)) {
                File::delete($this->getImagePath(null, true) . $oldImage);
            }
            
            return $name;
        }
        
        return $oldImage;
    }

}
