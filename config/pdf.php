<?php

return [
	'mode'                 => '',
	'format'               => 'A4-L',
	'default_font_size'    => '9',
	'default_font'         => 'sans-serif',
	'margin_left'          => 10,
	'margin_right'         => 10,
	'margin_top'           => 40,
	'margin_bottom'        => 20,
	'margin_header'        => 0,
	'margin_footer'        => 0,
	'orientation'          => 'L',
	'title'                => 'Laporan',
	'author'               => '',
	'watermark'            => '',
	'show_watermark'       => false,
	'watermark_font'       => 'sans-serif',
	'display_mode'         => 'fullpage',
	'watermark_text_alpha' => 0.1
];