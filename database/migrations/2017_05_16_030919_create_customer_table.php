<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama', 50)->nullable();
			$table->string('no_telp', 20)->nullable();
			$table->string('email', 150)->nullable();
			$table->string('password', 100)->nullable();
			$table->string('alamat', 100)->nullable()->default('');
			$table->string('tipe_id', 20)->nullable();
			$table->string('no_id', 30)->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->integer('status')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer');
	}

}
