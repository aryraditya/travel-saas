<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLanggananMemberTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('langganan_member', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_member')->unsigned()->nullable()->index('id_member');
			$table->integer('id_layanan')->unsigned()->nullable()->index('id_layanan');
			$table->integer('total')->nullable();
			$table->string('konfirmasi_ke_bank', 50)->nullable();
			$table->string('konfirmasi_dari_bank', 50)->nullable();
			$table->dateTime('tgl_bayar')->nullable();
			$table->integer('status')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('langganan_member');
	}

}
