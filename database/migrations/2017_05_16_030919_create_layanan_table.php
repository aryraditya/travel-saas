<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLayananTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('layanan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama', 50)->nullable();
			$table->integer('durasi')->nullable();
			$table->float('harga', 10, 0)->nullable();
			$table->text('keterangan', 65535)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('layanan');
	}

}
