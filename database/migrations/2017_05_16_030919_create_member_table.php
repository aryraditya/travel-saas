<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMemberTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('member', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('nama_usaha', 50)->nullable();
			$table->string('alamat', 100)->nullable();
			$table->string('kode_pos', 10)->nullable();
			$table->string('nama_pemilik', 50)->nullable();
			$table->string('no_telp', 20)->nullable();
			$table->text('deskripsi', 65535)->nullable();
			$table->string('email', 150)->nullable();
			$table->string('password', 100)->nullable();
			$table->integer('status')->nullable();
			$table->integer('id_layanan')->unsigned()->nullable();
			$table->dateTime('tgl_aktif')->nullable();
			$table->date('tgl_tenggang')->nullable();
			$table->date('tgl_disable')->nullable();
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('member');
	}

}
