<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePaketTourTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('paket_tour', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_member')->unsigned()->index('id_member');
			$table->string('nama', 100)->default('');
			$table->text('deskripsi', 65535);
			$table->integer('wilayah')->nullable();
			$table->float('harga', 10, 0)->nullable();
			$table->string('gambar_1')->nullable();
			$table->string('gambar_2')->nullable();
			$table->string('gambar_3')->nullable();
			$table->integer('status')->nullable();
			$table->integer('min_pax')->default(1);
			$table->integer('max_pax')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('paket_tour');
	}

}
