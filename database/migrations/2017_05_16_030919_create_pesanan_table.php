<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePesananTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('pesanan', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_cust')->unsigned()->nullable()->index('id_cust');
			$table->integer('id_tour')->unsigned()->nullable()->index('id_tour');
			$table->date('tgl_tour')->nullable();
			$table->date('tgl_bayar')->nullable();
			$table->integer('total_pax')->nullable();
			$table->float('total', 10, 0)->nullable();
			$table->string('metode_bayar', 50)->nullable();
			$table->string('cust_nama', 50)->nullable();
			$table->string('cust_no_telp', 20)->nullable();
			$table->string('cust_alamat', 100)->nullable();
			$table->string('cust_tipe_id', 20)->nullable();
			$table->string('cust_no_id', 30)->nullable();
			$table->string('cust_email', 150)->nullable();
			$table->string('konfirmasi_ke_bank', 50)->nullable();
			$table->string('konfirmasi_dari_bank', 50)->nullable();
			$table->integer('status')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('pesanan');
	}

}
