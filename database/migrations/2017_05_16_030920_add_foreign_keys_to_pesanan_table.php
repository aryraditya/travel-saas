<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPesananTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('pesanan', function(Blueprint $table)
		{
			$table->foreign('id_cust', 'pesanan_ibfk_1')->references('id')->on('customer')->onUpdate('CASCADE')->onDelete('CASCADE');
			$table->foreign('id_tour', 'pesanan_ibfk_2')->references('id')->on('paket_tour')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('pesanan', function(Blueprint $table)
		{
			$table->dropForeign('pesanan_ibfk_1');
			$table->dropForeign('pesanan_ibfk_2');
		});
	}

}
