<?php

use Illuminate\Database\Seeder;

class DefaultUser extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('admin')->insert([
            'status'    => 1,
            'nama'  => 'Adminstrator',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
        ]);
    }

}
