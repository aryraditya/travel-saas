# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: skr_cloudtour
# Generation Time: 2017-05-04 13:11:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`id`, `nama`, `email`, `password`, `remember_token`, `status`)
VALUES
	(1,'Adminstrator','admin@admin.com','$2y$10$9gk4fcDXtLrOmRwCXF4dYOr9KL1UwLCN4gbuteOSU.dDPaY6zCrNq',NULL,1),
	(2,'Demo','demo@demo.coms','$2y$10$AQa5tJKnwGe8depa4HglE.BBqyZQLrM7TfGUhHwp0bs8OfbOZpEnO',NULL,1);

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `tipe_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;

INSERT INTO `customer` (`id`, `nama`, `no_telp`, `email`, `password`, `alamat`, `tipe_id`, `no_id`, `remember_token`, `status`)
VALUES
	(1,'Demo Customer','085222','demo.customer@yuto.id','$2y$10$AQa5tJKnwGe8depa4HglE.BBqyZQLrM7TfGUhHwp0bs8OfbOZpEnO','Jalan Kaki','KTP','08322239932',NULL,1);

/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table layanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `layanan`;

CREATE TABLE `layanan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `layanan` WRITE;
/*!40000 ALTER TABLE `layanan` DISABLE KEYS */;

INSERT INTO `layanan` (`id`, `nama`, `durasi`, `harga`, `keterangan`, `created_at`, `updated_at`)
VALUES
	(1,'Basic',3,300000,NULL,'2017-05-02 09:44:49','2017-05-02 09:44:49'),
	(2,'Pro',6,500000,NULL,'2017-05-02 09:47:42','2017-05-02 09:47:42');

/*!40000 ALTER TABLE `layanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table layanan_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `layanan_member`;

CREATE TABLE `layanan_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_member` int(10) unsigned DEFAULT NULL,
  `id_layanan` int(10) unsigned DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `konfirmasi_ke_bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `konfirmasi_dari_bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `layanan_member` WRITE;
/*!40000 ALTER TABLE `layanan_member` DISABLE KEYS */;

INSERT INTO `layanan_member` (`id`, `id_member`, `id_layanan`, `total`, `konfirmasi_ke_bank`, `konfirmasi_dari_bank`, `tgl_bayar`, `status`, `created_at`, `updated_at`)
VALUES
	(1,3,1,300000,'Mandiri','MANDIRI-0283828239232','2017-05-03 04:07:33',10,'2017-05-03 02:57:34','2017-05-04 12:54:50'),
	(2,4,1,300000,'Mandiri','MANDIRI-02838282393','2017-05-04 13:06:05',10,'2017-05-04 13:05:44','2017-05-04 13:06:32');

/*!40000 ALTER TABLE `layanan_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_usaha` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_pemilik` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_layanan` int(10) unsigned DEFAULT NULL,
  `tgl_aktif` datetime DEFAULT NULL,
  `tgl_tenggang` date DEFAULT NULL,
  `tgl_disable` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;

INSERT INTO `member` (`id`, `nama_usaha`, `alamat`, `kode_pos`, `nama_pemilik`, `no_telp`, `deskripsi`, `email`, `password`, `status`, `id_layanan`, `tgl_aktif`, `tgl_tenggang`, `tgl_disable`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(2,'Demo Usaha','Jalan Kaki','20003','Demo','08560000333',NULL,'demo@member.com','$2y$10$V6mf2ynxGb/dJgoTjJSMw.q8eWtaBdwRoAywviTIN.SMPbZweU./u',1,1,'2017-05-02 10:03:27','2017-09-03','2017-09-04',NULL,'2017-05-02 10:03:27','2017-05-02 10:07:46'),
	(3,'Yuto Tour Bali','Jalan Gatot Subroto Timur No 522, Denpasar, Bali','82223','Demo Pemilik','085666660000',NULL,'demo.member2@yuto.id','$2y$10$6dSHvoUPlBqOcElZu6Q26uZ.ii1GD5rjdWqgO.HgXsuCGj7VqLw2S',1,1,'2017-05-04 12:54:50','2018-02-03','2018-03-03','O6cTExx3h74KRFUeM0u9Lc9XzFtDuw6GWlCqu69aB661DXdjrzg9puDtEMFA','2017-05-03 02:57:34','2017-05-04 12:54:50'),
	(4,'Demo demo','Jalan Kaki','213123','Demo 2','08232',NULL,'demo2@yuto.id','$2y$10$9Yqq3TPbeNJxxGhOyFuTNOgjLvKgxW68rhoBpwZkXxQaWFbJIDCUG',1,1,'2017-05-04 13:06:32','2017-08-04','2017-09-04',NULL,'2017-05-04 13:05:44','2017-05-04 13:06:32');

/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(13,'2017_05_01_131034_create_admin_table',1),
	(14,'2017_05_01_131034_create_customer_table',1),
	(15,'2017_05_01_131034_create_layanan_member_table',1),
	(16,'2017_05_01_131034_create_layanan_table',1),
	(17,'2017_05_01_131034_create_member_table',1),
	(18,'2017_05_01_131034_create_paket_tour_table',1),
	(19,'2017_05_01_131034_create_password_resets_table',1),
	(20,'2017_05_01_131034_create_pesanan_table',1),
	(21,'2017_05_01_131034_create_users_table',1),
	(22,'2017_05_01_131034_create_wilayah_table',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paket_tour
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paket_tour`;

CREATE TABLE `paket_tour` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_member` int(10) unsigned NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wilayah` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `gambar_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `paket_tour` WRITE;
/*!40000 ALTER TABLE `paket_tour` DISABLE KEYS */;

INSERT INTO `paket_tour` (`id`, `id_member`, `nama`, `deskripsi`, `wilayah`, `harga`, `gambar_1`, `gambar_2`, `gambar_3`, `status`, `created_at`, `updated_at`)
VALUES
	(1,3,'Diving di Lovina oleh Krisna Watersports','<h2>Alamat Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Desa Temukus, kec. Banjar, kab. Buleleng, Bali &ndash; Indonesia.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Deskripsi Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Bagi pecinta wisata bahari dan gemar menyelam di bawah laut, Bali merupakan salah satu tempat yang memiliki sejumlah lokasi menyelam yang indah dan menawan. Sejumlah tempat yang sudah terkenal sebagai spot tempat menyelam di Bali karena keindahan pemandangan bawah lautnya yaitu Lovina-Singaraja, Sanur, Menjangan, Amed-Karangasem, Tulamben, Amuk Bay dan Pantai Tanjung Benoa.<br />\r\n<br />\r\nNah, untuk spot diving di Lovina tidaklah terlalu sulit untuk mengaksesnya. Pantai Lovina berjarak sekitar 9 kilometer sebalah barat kota Singaraja, Buleleng dan merupakan salah satu tempat wisata yang ada di kawasan Bali Utara. Wisatawan baik yang lokal maupun mancanegara banyak yang berkunjung ke kawasan Lovina ini untuk menikmati beragam atraksi wisata yang disediakan sejumlah provider wisata termasuk untuk atraksi wisata diving atau menyelam.<br />\r\n<br />\r\nUntuk Anda yang tengah liburan di kawasan Singaraja dan berencana diving di kawasan Lovina maka tidak ada salahnya mencoba provider diving lokal terpercaya yang bernama Krisna Watersport. Anda bisa menikmati aktivitas bawah laut dengan program diving di Lovina bersama&nbsp;<a href=\"https://www.vokamo.com/id/krisna-adventure\" style=\"text-decoration-line: none; color: black;\">Krisna</a>&nbsp;Watersports dengan harga terhemat. Untuk pecinta diving jangan ragu booking paket diving di Lovina dengan harga promo tentunya.&nbsp;<br />\r\n&nbsp;</div>\r\n\r\n<h2>Yang Harus Anda Persiapkan</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Kartu Identitas, Uang Tunai, Kamera, Pakaian</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Syarat dan Ketentuan Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Harga yang tertera di website adalah untuk tamu lokal/domestik.</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Paket Diving</div>\r\n\r\n<div class=\"clear\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Tidak Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Fasilitas Tambahan</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Makanan/Minuman</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">&nbsp;</div>',1,300000,'diving-di-lovina-oleh-krisna-watersports_1493802153.jpg',NULL,NULL,1,'2017-05-03 08:56:09','2017-05-03 09:02:33');

/*!40000 ALTER TABLE `paket_tour` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table pesanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pesanan`;

CREATE TABLE `pesanan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cust` int(10) unsigned DEFAULT NULL,
  `id_tour` int(10) unsigned DEFAULT NULL,
  `tgl_tour` date DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `total_pax` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `metode_bayar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pesanan` WRITE;
/*!40000 ALTER TABLE `pesanan` DISABLE KEYS */;

INSERT INTO `pesanan` (`id`, `id_cust`, `id_tour`, `tgl_tour`, `tgl_bayar`, `total_pax`, `total`, `metode_bayar`, `status`, `created_at`, `updated_at`)
VALUES
	(1,1,1,'2017-05-10','2017-05-05',2,600000,'transfer',10,'2017-05-05 00:00:00','2017-05-05 00:00:00');

/*!40000 ALTER TABLE `pesanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status` int(11) NOT NULL,
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(125) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wilayah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wilayah`;

CREATE TABLE `wilayah` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wilayah` WRITE;
/*!40000 ALTER TABLE `wilayah` DISABLE KEYS */;

INSERT INTO `wilayah` (`id`, `type`, `nama`, `parent_id`, `created_at`, `updated_at`)
VALUES
	(1,'','Bali',NULL,'2017-05-03 02:13:00','2017-05-03 02:13:00'),
	(3,'','Yogyakarta',NULL,'2017-05-03 02:13:13','2017-05-03 02:13:13'),
	(4,'','Jakarta',NULL,'2017-05-03 02:14:03','2017-05-03 02:14:03'),
	(5,'','Nusa Lembongan',NULL,'2017-05-03 02:14:16','2017-05-03 02:14:16'),
	(6,'','Pulau Komodo',NULL,'2017-05-03 02:14:24','2017-05-03 02:14:24'),
	(7,'','Gili Trawangan',NULL,'2017-05-03 02:15:55','2017-05-03 02:15:55'),
	(8,'','Solo',NULL,'2017-05-03 02:16:06','2017-05-03 02:16:06');

/*!40000 ALTER TABLE `wilayah` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
