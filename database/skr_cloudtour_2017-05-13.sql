# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.6.25)
# Database: skr_cloudtour
# Generation Time: 2017-05-13 04:03:06 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`id`, `nama`, `email`, `password`, `remember_token`, `status`)
VALUES
	(1,'Adminstrator','admin@admin.com','$2y$10$9gk4fcDXtLrOmRwCXF4dYOr9KL1UwLCN4gbuteOSU.dDPaY6zCrNq',NULL,1),
	(2,'Demo','demo@demo.coms','$2y$10$AQa5tJKnwGe8depa4HglE.BBqyZQLrM7TfGUhHwp0bs8OfbOZpEnO',NULL,1);

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table bank_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `bank_member`;

CREATE TABLE `bank_member` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_member` int(11) unsigned NOT NULL,
  `nama_bank` varchar(30) DEFAULT NULL,
  `no_rek` varchar(50) DEFAULT NULL,
  `kcp` varchar(50) DEFAULT NULL,
  `atas_nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_member` (`id_member`),
  CONSTRAINT `bank_member_ibfk_1` FOREIGN KEY (`id_member`) REFERENCES `member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `bank_member` WRITE;
/*!40000 ALTER TABLE `bank_member` DISABLE KEYS */;

INSERT INTO `bank_member` (`id`, `id_member`, `nama_bank`, `no_rek`, `kcp`, `atas_nama`)
VALUES
	(1,3,'BCA','02329292732','Denpasar','PT. Yuk Tour Asia'),
	(3,3,'MANDIRI','02888332929','Denpasar',NULL);

/*!40000 ALTER TABLE `bank_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table customer
# ------------------------------------------------------------

DROP TABLE IF EXISTS `customer`;

CREATE TABLE `customer` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `tipe_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;

INSERT INTO `customer` (`id`, `nama`, `no_telp`, `email`, `password`, `alamat`, `tipe_id`, `no_id`, `remember_token`, `status`)
VALUES
	(1,'Demo Customer','085222','demo.customer@yuto.id','$2y$10$AQa5tJKnwGe8depa4HglE.BBqyZQLrM7TfGUhHwp0bs8OfbOZpEnO','Jalan Kaki','KTP','08322239932','Yu33K6eIIv8n50hVqFVzg5cOU1f0kpeDsO7bZcSl3P5FFBafJRGdI8brRoEM',1),
	(4,'Demo Customers','082382822','demo.customer@gmail.com','$2y$10$AQa5tJKnwGe8depa4HglE.BBqyZQLrM7TfGUhHwp0bs8OfbOZpEnO','Jalan Kaki','SIM','2932882821233','xf8Z5GSGUmHADmMtCus7jO8026t2l6LX6BlHoXAj1Jv7b3CtUBKbA44pjU8y',1);

/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table langganan_member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `langganan_member`;

CREATE TABLE `langganan_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_member` int(10) unsigned DEFAULT NULL,
  `id_layanan` int(10) unsigned DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `konfirmasi_ke_bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `konfirmasi_dari_bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `status` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_member` (`id_member`),
  KEY `id_layanan` (`id_layanan`),
  CONSTRAINT `langganan_member_ibfk_2` FOREIGN KEY (`id_member`) REFERENCES `member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `langganan_member_ibfk_3` FOREIGN KEY (`id_layanan`) REFERENCES `layanan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `langganan_member` WRITE;
/*!40000 ALTER TABLE `langganan_member` DISABLE KEYS */;

INSERT INTO `langganan_member` (`id`, `id_member`, `id_layanan`, `total`, `konfirmasi_ke_bank`, `konfirmasi_dari_bank`, `tgl_bayar`, `status`, `created_at`, `updated_at`)
VALUES
	(1,3,1,300000,'Mandiri','MANDIRI-0283828239232','2017-05-03 04:07:33',10,'2017-05-03 02:57:34','2017-05-04 12:54:50'),
	(2,4,1,300000,'Mandiri','MANDIRI-02838282393','2017-05-04 13:06:05',10,'2017-05-04 13:05:44','2017-05-04 13:06:32'),
	(6,3,1,300000,NULL,NULL,NULL,0,'2017-05-12 07:48:37','2017-05-12 07:48:37'),
	(7,3,1,300000,'BCA','BCA-923929292922','2017-05-12 07:50:47',10,'2017-05-12 07:50:26','2017-05-12 07:51:24');

/*!40000 ALTER TABLE `langganan_member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table layanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `layanan`;

CREATE TABLE `layanan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `keterangan` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `layanan` WRITE;
/*!40000 ALTER TABLE `layanan` DISABLE KEYS */;

INSERT INTO `layanan` (`id`, `nama`, `durasi`, `harga`, `keterangan`, `created_at`, `updated_at`)
VALUES
	(1,'Basic',3,300000,NULL,'2017-05-02 09:44:49','2017-05-02 09:44:49'),
	(2,'Pro',6,500000,NULL,'2017-05-02 09:47:42','2017-05-02 09:47:42');

/*!40000 ALTER TABLE `layanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table member
# ------------------------------------------------------------

DROP TABLE IF EXISTS `member`;

CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama_usaha` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_pos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_pemilik` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_telp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `id_layanan` int(10) unsigned DEFAULT NULL,
  `tgl_aktif` datetime DEFAULT NULL,
  `tgl_tenggang` date DEFAULT NULL,
  `tgl_disable` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `member` WRITE;
/*!40000 ALTER TABLE `member` DISABLE KEYS */;

INSERT INTO `member` (`id`, `nama_usaha`, `alamat`, `kode_pos`, `nama_pemilik`, `no_telp`, `deskripsi`, `email`, `password`, `status`, `id_layanan`, `tgl_aktif`, `tgl_tenggang`, `tgl_disable`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(2,'Demo Usaha','Jalan Kaki','20003','Demo','08560000333',NULL,'demo@member.com','$2y$10$V6mf2ynxGb/dJgoTjJSMw.q8eWtaBdwRoAywviTIN.SMPbZweU./u',1,1,'2017-05-02 10:03:27','2017-09-03','2017-09-04',NULL,'2017-05-02 10:03:27','2017-05-02 10:07:46'),
	(3,'Yuto Tour Bali','Jalan Gatot Subroto Timur No 522, Denpasar, Bali','82223','Demo Pemilik','085666660000',NULL,'demo.member2@yuto.id','$2y$10$6dSHvoUPlBqOcElZu6Q26uZ.ii1GD5rjdWqgO.HgXsuCGj7VqLw2S',1,1,'2017-05-12 07:51:24','2017-08-12','2017-09-12','O6cTExx3h74KRFUeM0u9Lc9XzFtDuw6GWlCqu69aB661DXdjrzg9puDtEMFA','2017-05-03 02:57:34','2017-05-12 07:51:24'),
	(4,'Demo demo','Jalan Kaki','213123','Demo 2','08232',NULL,'demo2@yuto.id','$2y$10$9Yqq3TPbeNJxxGhOyFuTNOgjLvKgxW68rhoBpwZkXxQaWFbJIDCUG',1,1,'2017-05-04 13:06:32','2017-08-04','2017-09-04',NULL,'2017-05-04 13:05:44','2017-05-04 13:06:32');

/*!40000 ALTER TABLE `member` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(23,'2017_05_10_014630_create_admin_table',0),
	(24,'2017_05_10_014630_create_bank_member_table',0),
	(25,'2017_05_10_014630_create_customer_table',0),
	(26,'2017_05_10_014630_create_langganan_member_table',0),
	(27,'2017_05_10_014630_create_layanan_table',0),
	(28,'2017_05_10_014630_create_member_table',0),
	(29,'2017_05_10_014630_create_paket_tour_table',0),
	(30,'2017_05_10_014630_create_pesanan_table',0),
	(31,'2017_05_10_014630_create_wilayah_table',0),
	(32,'2017_05_10_014631_add_foreign_keys_to_bank_member_table',0),
	(33,'2017_05_10_014631_add_foreign_keys_to_langganan_member_table',0),
	(34,'2017_05_10_014631_add_foreign_keys_to_paket_tour_table',0),
	(35,'2017_05_10_014631_add_foreign_keys_to_pesanan_table',0);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table paket_tour
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paket_tour`;

CREATE TABLE `paket_tour` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_member` int(10) unsigned NOT NULL,
  `nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `wilayah` int(11) DEFAULT NULL,
  `harga` double DEFAULT NULL,
  `gambar_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gambar_3` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_member` (`id_member`),
  CONSTRAINT `paket_tour_ibfk_1` FOREIGN KEY (`id_member`) REFERENCES `member` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `paket_tour` WRITE;
/*!40000 ALTER TABLE `paket_tour` DISABLE KEYS */;

INSERT INTO `paket_tour` (`id`, `id_member`, `nama`, `deskripsi`, `wilayah`, `harga`, `gambar_1`, `gambar_2`, `gambar_3`, `status`, `created_at`, `updated_at`)
VALUES
	(1,3,'Diving di Lovina oleh Krisna Watersports','<h2>Alamat Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Desa Temukus, kec. Banjar, kab. Buleleng, Bali &ndash; Indonesia.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Deskripsi Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Bagi pecinta wisata bahari dan gemar menyelam di bawah laut, Bali merupakan salah satu tempat yang memiliki sejumlah lokasi menyelam yang indah dan menawan. Sejumlah tempat yang sudah terkenal sebagai spot tempat menyelam di Bali karena keindahan pemandangan bawah lautnya yaitu Lovina-Singaraja, Sanur, Menjangan, Amed-Karangasem, Tulamben, Amuk Bay dan Pantai Tanjung Benoa.<br />\r\n<br />\r\nNah, untuk spot diving di Lovina tidaklah terlalu sulit untuk mengaksesnya. Pantai Lovina berjarak sekitar 9 kilometer sebalah barat kota Singaraja, Buleleng dan merupakan salah satu tempat wisata yang ada di kawasan Bali Utara. Wisatawan baik yang lokal maupun mancanegara banyak yang berkunjung ke kawasan Lovina ini untuk menikmati beragam atraksi wisata yang disediakan sejumlah provider wisata termasuk untuk atraksi wisata diving atau menyelam.<br />\r\n<br />\r\nUntuk Anda yang tengah liburan di kawasan Singaraja dan berencana diving di kawasan Lovina maka tidak ada salahnya mencoba provider diving lokal terpercaya yang bernama Krisna Watersport. Anda bisa menikmati aktivitas bawah laut dengan program diving di Lovina bersama&nbsp;<a href=\"https://www.vokamo.com/id/krisna-adventure\" style=\"text-decoration-line: none; color: black;\">Krisna</a>&nbsp;Watersports dengan harga terhemat. Untuk pecinta diving jangan ragu booking paket diving di Lovina dengan harga promo tentunya.&nbsp;<br />\r\n&nbsp;</div>\r\n\r\n<h2>Yang Harus Anda Persiapkan</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Kartu Identitas, Uang Tunai, Kamera, Pakaian</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Syarat dan Ketentuan Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Harga yang tertera di website adalah untuk tamu lokal/domestik.</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Paket Diving</div>\r\n\r\n<div class=\"clear\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Tidak Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Fasilitas Tambahan</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Makanan/Minuman</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">&nbsp;</div>',1,300000,'diving-di-lovina-oleh-krisna-watersports_1494299474.jpg',NULL,NULL,1,'2017-05-03 08:56:09','2017-05-09 03:11:14'),
	(2,3,'4D Bali Art & Culture','<h2>Alamat Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Desa Temukus, kec. Banjar, kab. Buleleng, Bali &ndash; Indonesia.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Deskripsi Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Bagi pecinta wisata bahari dan gemar menyelam di bawah laut, Bali merupakan salah satu tempat yang memiliki sejumlah lokasi menyelam yang indah dan menawan. Sejumlah tempat yang sudah terkenal sebagai spot tempat menyelam di Bali karena keindahan pemandangan bawah lautnya yaitu Lovina-Singaraja, Sanur, Menjangan, Amed-Karangasem, Tulamben, Amuk Bay dan Pantai Tanjung Benoa.<br />\r\n<br />\r\nNah, untuk spot diving di Lovina tidaklah terlalu sulit untuk mengaksesnya. Pantai Lovina berjarak sekitar 9 kilometer sebalah barat kota Singaraja, Buleleng dan merupakan salah satu tempat wisata yang ada di kawasan Bali Utara. Wisatawan baik yang lokal maupun mancanegara banyak yang berkunjung ke kawasan Lovina ini untuk menikmati beragam atraksi wisata yang disediakan sejumlah provider wisata termasuk untuk atraksi wisata diving atau menyelam.<br />\r\n<br />\r\nUntuk Anda yang tengah liburan di kawasan Singaraja dan berencana diving di kawasan Lovina maka tidak ada salahnya mencoba provider diving lokal terpercaya yang bernama Krisna Watersport. Anda bisa menikmati aktivitas bawah laut dengan program diving di Lovina bersama&nbsp;<a href=\"https://www.vokamo.com/id/krisna-adventure\" style=\"text-decoration-line: none; color: black;\">Krisna</a>&nbsp;Watersports dengan harga terhemat. Untuk pecinta diving jangan ragu booking paket diving di Lovina dengan harga promo tentunya.&nbsp;<br />\r\n&nbsp;</div>\r\n\r\n<h2>Yang Harus Anda Persiapkan</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Kartu Identitas, Uang Tunai, Kamera, Pakaian</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Syarat dan Ketentuan Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Harga yang tertera di website adalah untuk tamu lokal/domestik.</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Paket Diving</div>\r\n\r\n<div class=\"clear\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Tidak Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Fasilitas Tambahan</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Makanan/Minuman</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">&nbsp;</div>',1,2200000,'4d-bali-art-culture_1494298981.jpg',NULL,NULL,1,'2017-05-03 08:56:09','2017-05-09 03:03:01'),
	(3,3,'Explore Pulau Komodo - Pink Beach','<h2>Alamat Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Desa Temukus, kec. Banjar, kab. Buleleng, Bali &ndash; Indonesia.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Deskripsi Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Bagi pecinta wisata bahari dan gemar menyelam di bawah laut, Bali merupakan salah satu tempat yang memiliki sejumlah lokasi menyelam yang indah dan menawan. Sejumlah tempat yang sudah terkenal sebagai spot tempat menyelam di Bali karena keindahan pemandangan bawah lautnya yaitu Lovina-Singaraja, Sanur, Menjangan, Amed-Karangasem, Tulamben, Amuk Bay dan Pantai Tanjung Benoa.<br />\r\n<br />\r\nNah, untuk spot diving di Lovina tidaklah terlalu sulit untuk mengaksesnya. Pantai Lovina berjarak sekitar 9 kilometer sebalah barat kota Singaraja, Buleleng dan merupakan salah satu tempat wisata yang ada di kawasan Bali Utara. Wisatawan baik yang lokal maupun mancanegara banyak yang berkunjung ke kawasan Lovina ini untuk menikmati beragam atraksi wisata yang disediakan sejumlah provider wisata termasuk untuk atraksi wisata diving atau menyelam.<br />\r\n<br />\r\nUntuk Anda yang tengah liburan di kawasan Singaraja dan berencana diving di kawasan Lovina maka tidak ada salahnya mencoba provider diving lokal terpercaya yang bernama Krisna Watersport. Anda bisa menikmati aktivitas bawah laut dengan program diving di Lovina bersama&nbsp;<a href=\"https://www.vokamo.com/id/krisna-adventure\" style=\"text-decoration-line: none; color: black;\">Krisna</a>&nbsp;Watersports dengan harga terhemat. Untuk pecinta diving jangan ragu booking paket diving di Lovina dengan harga promo tentunya.&nbsp;<br />\r\n&nbsp;</div>\r\n\r\n<h2>Yang Harus Anda Persiapkan</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Kartu Identitas, Uang Tunai, Kamera, Pakaian</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Syarat dan Ketentuan Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Harga yang tertera di website adalah untuk tamu lokal/domestik.</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Paket Diving</div>\r\n\r\n<div class=\"clear\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Tidak Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Fasilitas Tambahan</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Makanan/Minuman</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">&nbsp;</div>',6,300000,'explore-pulau-komodo-pink-beach_1494299200.jpg',NULL,NULL,1,'2017-05-03 08:56:09','2017-05-09 03:06:40'),
	(4,3,'Diving di Lovina oleh Krisna Watersports BALI UTARA - SINGARAJA','<h2>Alamat Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Desa Temukus, kec. Banjar, kab. Buleleng, Bali &ndash; Indonesia.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Deskripsi Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Bagi pecinta wisata bahari dan gemar menyelam di bawah laut, Bali merupakan salah satu tempat yang memiliki sejumlah lokasi menyelam yang indah dan menawan. Sejumlah tempat yang sudah terkenal sebagai spot tempat menyelam di Bali karena keindahan pemandangan bawah lautnya yaitu Lovina-Singaraja, Sanur, Menjangan, Amed-Karangasem, Tulamben, Amuk Bay dan Pantai Tanjung Benoa.<br />\r\n<br />\r\nNah, untuk spot diving di Lovina tidaklah terlalu sulit untuk mengaksesnya. Pantai Lovina berjarak sekitar 9 kilometer sebalah barat kota Singaraja, Buleleng dan merupakan salah satu tempat wisata yang ada di kawasan Bali Utara. Wisatawan baik yang lokal maupun mancanegara banyak yang berkunjung ke kawasan Lovina ini untuk menikmati beragam atraksi wisata yang disediakan sejumlah provider wisata termasuk untuk atraksi wisata diving atau menyelam.<br />\r\n<br />\r\nUntuk Anda yang tengah liburan di kawasan Singaraja dan berencana diving di kawasan Lovina maka tidak ada salahnya mencoba provider diving lokal terpercaya yang bernama Krisna Watersport. Anda bisa menikmati aktivitas bawah laut dengan program diving di Lovina bersama&nbsp;<a href=\"https://www.vokamo.com/id/krisna-adventure\" style=\"text-decoration-line: none; color: black;\">Krisna</a>&nbsp;Watersports dengan harga terhemat. Untuk pecinta diving jangan ragu booking paket diving di Lovina dengan harga promo tentunya.&nbsp;<br />\r\n&nbsp;</div>\r\n\r\n<h2>Yang Harus Anda Persiapkan</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Kartu Identitas, Uang Tunai, Kamera, Pakaian</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Syarat dan Ketentuan Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Harga yang tertera di website adalah untuk tamu lokal/domestik.</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Paket Diving</div>\r\n\r\n<div class=\"clear\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Tidak Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Fasilitas Tambahan</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Makanan/Minuman</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">&nbsp;</div>',1,300000,'diving-di-lovina-oleh-krisna-watersports-bali-utara-singaraja_1494299444.jpg','diving-di-lovina-oleh-krisna-watersports-bali-utara-singaraja_1494299458.jpg',NULL,1,'2017-05-03 08:56:09','2017-05-09 03:10:58'),
	(5,3,'Tour ke bali 3D 2N','<h2>Alamat Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Desa Temukus, kec. Banjar, kab. Buleleng, Bali &ndash; Indonesia.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Deskripsi Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Bagi pecinta wisata bahari dan gemar menyelam di bawah laut, Bali merupakan salah satu tempat yang memiliki sejumlah lokasi menyelam yang indah dan menawan. Sejumlah tempat yang sudah terkenal sebagai spot tempat menyelam di Bali karena keindahan pemandangan bawah lautnya yaitu Lovina-Singaraja, Sanur, Menjangan, Amed-Karangasem, Tulamben, Amuk Bay dan Pantai Tanjung Benoa.<br />\r\n<br />\r\nNah, untuk spot diving di Lovina tidaklah terlalu sulit untuk mengaksesnya. Pantai Lovina berjarak sekitar 9 kilometer sebalah barat kota Singaraja, Buleleng dan merupakan salah satu tempat wisata yang ada di kawasan Bali Utara. Wisatawan baik yang lokal maupun mancanegara banyak yang berkunjung ke kawasan Lovina ini untuk menikmati beragam atraksi wisata yang disediakan sejumlah provider wisata termasuk untuk atraksi wisata diving atau menyelam.<br />\r\n<br />\r\nUntuk Anda yang tengah liburan di kawasan Singaraja dan berencana diving di kawasan Lovina maka tidak ada salahnya mencoba provider diving lokal terpercaya yang bernama Krisna Watersport. Anda bisa menikmati aktivitas bawah laut dengan program diving di Lovina bersama&nbsp;<a href=\"https://www.vokamo.com/id/krisna-adventure\" style=\"text-decoration-line: none; color: black;\">Krisna</a>&nbsp;Watersports dengan harga terhemat. Untuk pecinta diving jangan ragu booking paket diving di Lovina dengan harga promo tentunya.&nbsp;<br />\r\n&nbsp;</div>\r\n\r\n<h2>Yang Harus Anda Persiapkan</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Kartu Identitas, Uang Tunai, Kamera, Pakaian</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Syarat dan Ketentuan Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Harga yang tertera di website adalah untuk tamu lokal/domestik.</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Paket Diving</div>\r\n\r\n<div class=\"clear\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Tidak Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Fasilitas Tambahan</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Makanan/Minuman</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">&nbsp;</div>',1,1200000,'tour-ke-bali-3d-2n_1494298169.jpg',NULL,NULL,1,'2017-05-03 08:56:09','2017-05-09 02:49:34'),
	(6,3,'Diving di Lovina oleh Krisna Watersports','<h2>Alamat Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Desa Temukus, kec. Banjar, kab. Buleleng, Bali &ndash; Indonesia.</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h2>Deskripsi Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Bagi pecinta wisata bahari dan gemar menyelam di bawah laut, Bali merupakan salah satu tempat yang memiliki sejumlah lokasi menyelam yang indah dan menawan. Sejumlah tempat yang sudah terkenal sebagai spot tempat menyelam di Bali karena keindahan pemandangan bawah lautnya yaitu Lovina-Singaraja, Sanur, Menjangan, Amed-Karangasem, Tulamben, Amuk Bay dan Pantai Tanjung Benoa.<br />\r\n<br />\r\nNah, untuk spot diving di Lovina tidaklah terlalu sulit untuk mengaksesnya. Pantai Lovina berjarak sekitar 9 kilometer sebalah barat kota Singaraja, Buleleng dan merupakan salah satu tempat wisata yang ada di kawasan Bali Utara. Wisatawan baik yang lokal maupun mancanegara banyak yang berkunjung ke kawasan Lovina ini untuk menikmati beragam atraksi wisata yang disediakan sejumlah provider wisata termasuk untuk atraksi wisata diving atau menyelam.<br />\r\n<br />\r\nUntuk Anda yang tengah liburan di kawasan Singaraja dan berencana diving di kawasan Lovina maka tidak ada salahnya mencoba provider diving lokal terpercaya yang bernama Krisna Watersport. Anda bisa menikmati aktivitas bawah laut dengan program diving di Lovina bersama&nbsp;<a href=\"https://www.vokamo.com/id/krisna-adventure\" style=\"text-decoration-line: none; color: black;\">Krisna</a>&nbsp;Watersports dengan harga terhemat. Untuk pecinta diving jangan ragu booking paket diving di Lovina dengan harga promo tentunya.&nbsp;<br />\r\n&nbsp;</div>\r\n\r\n<h2>Yang Harus Anda Persiapkan</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Kartu Identitas, Uang Tunai, Kamera, Pakaian</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Syarat dan Ketentuan Diving di Lovina oleh Krisna Watersports</h2>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">Harga yang tertera di website adalah untuk tamu lokal/domestik.</div>\r\n\r\n<div class=\"fulldescription\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Paket Diving</div>\r\n\r\n<div class=\"clear\" style=\"font-family: \">&nbsp;</div>\r\n\r\n<h2>Tidak Termasuk</h2>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Fasilitas Tambahan</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">Makanan/Minuman</div>\r\n\r\n<div class=\"check descfac\" style=\"font-family: \">&nbsp;</div>',1,300000,'diving-di-lovina-oleh-krisna-watersports_1494298479.jpg',NULL,NULL,1,'2017-05-03 08:56:09','2017-05-09 02:54:39');

/*!40000 ALTER TABLE `paket_tour` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pesanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pesanan`;

CREATE TABLE `pesanan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_cust` int(10) unsigned DEFAULT NULL,
  `id_tour` int(10) unsigned DEFAULT NULL,
  `tgl_tour` date DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `total_pax` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `metode_bayar` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cust_nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cust_no_telp` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cust_alamat` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cust_tipe_id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cust_no_id` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cust_email` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `konfirmasi_ke_bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `konfirmasi_dari_bank` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_cust` (`id_cust`),
  KEY `id_tour` (`id_tour`),
  CONSTRAINT `pesanan_ibfk_1` FOREIGN KEY (`id_cust`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `pesanan_ibfk_2` FOREIGN KEY (`id_tour`) REFERENCES `paket_tour` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `pesanan` WRITE;
/*!40000 ALTER TABLE `pesanan` DISABLE KEYS */;

INSERT INTO `pesanan` (`id`, `id_cust`, `id_tour`, `tgl_tour`, `tgl_bayar`, `total_pax`, `total`, `metode_bayar`, `cust_nama`, `cust_no_telp`, `cust_alamat`, `cust_tipe_id`, `cust_no_id`, `cust_email`, `konfirmasi_ke_bank`, `konfirmasi_dari_bank`, `status`, `created_at`, `updated_at`)
VALUES
	(4,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-04-10 02:15:46','2017-05-10 03:58:40'),
	(5,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',0,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(6,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(7,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(8,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(9,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',5,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(10,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(11,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(12,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(13,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(14,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(15,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(16,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(17,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(18,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(19,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(20,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(21,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(22,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(23,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(24,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(25,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(26,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(27,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(28,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(29,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(30,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(31,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(32,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(33,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(34,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(35,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(36,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(37,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(38,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(39,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(40,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(41,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(42,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(43,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(44,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(45,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(46,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(47,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(48,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(49,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(50,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(51,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(52,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(53,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(54,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(55,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(56,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(57,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(58,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(59,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(60,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(61,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(62,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(63,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(64,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(65,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(66,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(67,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(68,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(69,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(70,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(71,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(72,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(73,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(74,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(75,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(76,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(77,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(78,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(79,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(80,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(81,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(82,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(83,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(84,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(85,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(86,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(87,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(88,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(89,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(90,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(91,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(92,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(93,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(94,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(95,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(96,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(97,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(98,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(99,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(100,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(101,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(102,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(103,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(104,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(105,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(106,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(107,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(108,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(109,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(110,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(111,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(112,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(113,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(114,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(115,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(116,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(117,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(118,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(119,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(120,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(121,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(122,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40'),
	(123,4,4,'2017-05-10','2017-05-10',2,600000,'transfer','Demo Customer','082382822','Jalan Kaki','KTP','2932882821233','demo.customer@gmail.com','MANDIRI - 08832227777332','MANDIRI - 28288333222',10,'2017-05-10 02:15:46','2017-05-10 03:58:40');

/*!40000 ALTER TABLE `pesanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wilayah
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wilayah`;

CREATE TABLE `wilayah` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wilayah` WRITE;
/*!40000 ALTER TABLE `wilayah` DISABLE KEYS */;

INSERT INTO `wilayah` (`id`, `type`, `nama`, `parent_id`, `created_at`, `updated_at`)
VALUES
	(1,'','Bali',NULL,'2017-05-03 02:13:00','2017-05-03 02:13:00'),
	(3,'','Yogyakarta',NULL,'2017-05-03 02:13:13','2017-05-03 02:13:13'),
	(4,'','Jakarta',NULL,'2017-05-03 02:14:03','2017-05-03 02:14:03'),
	(5,'','Nusa Lembongan',NULL,'2017-05-03 02:14:16','2017-05-03 02:14:16'),
	(6,'','Pulau Komodo',NULL,'2017-05-03 02:14:24','2017-05-03 02:14:24'),
	(7,'','Gili Trawangan',NULL,'2017-05-03 02:15:55','2017-05-03 02:15:55'),
	(8,'','Solo',NULL,'2017-05-03 02:16:06','2017-05-03 02:16:06'),
	(9,'','Bandung',NULL,'2017-05-08 14:39:39','2017-05-08 14:39:39'),
	(10,'','Malang',NULL,'2017-05-08 14:39:48','2017-05-08 14:39:48'),
	(11,'','Lombok',NULL,'2017-05-08 14:41:26','2017-05-08 14:41:26'),
	(12,'','Nusa Tenggara Barat',NULL,'2017-05-08 14:41:37','2017-05-08 14:41:37'),
	(13,'','Nusa Tenggara Timur',NULL,'2017-05-08 14:41:45','2017-05-08 14:41:45'),
	(14,'','Sumatra',NULL,'2017-05-08 14:42:11','2017-05-08 14:42:11'),
	(15,'','Papua',NULL,'2017-05-08 14:42:17','2017-05-08 14:42:17'),
	(16,'','Semarang',NULL,'2017-05-08 14:54:29','2017-05-08 14:54:29');

/*!40000 ALTER TABLE `wilayah` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
