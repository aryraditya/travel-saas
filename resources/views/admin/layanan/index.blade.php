@extends('layouts.dashboard.admin')

@section('title', 'Daftar Layanan')
@section('page-title', 'Daftar Layanan')

@section('content')
<div class="block">
    <div class="row mb15">
        <div class="col-md-6">
            <a href="{{ route('admin.layanan.create') }}" class="btn btn-warning">
                <i class="gi gi-plus"></i>
                Tambah Layanan
            </a>
        </div>
        <div class="col-md-6">
            <form class="form-inline" action="{{ route('admin.layanan.index') }}">
                <div  class="dataTables_filter">
                    <label>
                        <div class="input-group">
                            <input type="search" name="search" value="{{ Request::get('search') }}" class="form-control" aria-controls="example-datatable" placeholder="Cari...">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </label>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Layanan</th>
                    <th>Durasi</th>
                    <th>Harga</th>
                    <th>Keterangan</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($models as $key => $model)
                <tr>
                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1)}}</td>
                    <td>
                        <div class=""><strong>{{ $model->nama }}</strong></div> 
                    </td>
                    <td>{{ $model->durasi }} Bulan</td>
                    <td>Rp. {{ number_format($model->harga, 2,',','.') }}</td>
                    <td>{{ $model->keterangan }}</td>
                    <td class="text-right">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.layanan.edit', ['id' => $model->id]) }}" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach

                @if($models->count() == 0)
                <tr>
                    <td colspan="6">
                        <i>Tidak ada data untuk ditampilkan</i>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        {!! $models->appends(['search' => Request::get('search')])->links() !!}
    </div>
</div>
@endsection