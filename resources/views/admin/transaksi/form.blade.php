@extends('layouts.dashboard.admin')

@section('title', $model->exists ? 'Ubah Layanan' : 'Tambah Layanan')
@section('page-title',$model->exists ? 'Ubah Layanan' : 'Tambah Layanan')


@section('content')
<div class="block">
    <form action="{{ $model->exists ? route('admin.layanan.update', ['id' => $model->id]) : route('admin.layanan.store') }}" method="POST" class="form-horizontal form-bordered">
        {{         csrf_field() }}
        {{ $model->exists ? method_field('PUT') : method_field('POST') }}
        <div class="form-group">
            <label class="col-md-3 control-label">Nama</label>
            <div class="col-md-9">
                <input type="text" name="nama" class="form-control" value="{{ old('nama', $model->nama) }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Durasi</label>
            <div class="col-md-9">
                <div class="input-group">
                    <input type="text" name="durasi" class="form-control" value="{{ old('durasi', $model->durasi) }}">
                    <span class="input-group-addon">Bulan</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Harga</label>
            <div class="col-md-9">
                <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" name="harga" class="form-control" value="{{ old('harga', $model->harga) }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Keterangan</label>
            <div class="col-md-9">
                <textarea name="keterangan" class="form-control">{{ old('keterangan', $model->keterangan) }}</textarea>
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-sm btn-primary">
                    <i class="fa fa-angle-right"></i> 
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>


@endsection