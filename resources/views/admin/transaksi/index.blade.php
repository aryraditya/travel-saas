@extends('layouts.dashboard.admin')

@section('title', 'Daftar Transaksi Member')
@section('page-title', 'Daftar Transaksi Member')

@section('content')
<div class="block">
    <div class="row mb15">
        <div class="col-md-6">

        </div>
        <div class="col-md-6">
            <form class="form-inline" action="{{ route('admin.transaksi.index') }}">
                <div  class="dataTables_filter">
                    <label>
                        <div class="input-group">
                            <input type="search" name="search" value="{{ Request::get('search') }}" class="form-control" aria-controls="example-datatable" placeholder="Cari...">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </label>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID Transaksi</th>
                    <th>Member</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($models as $key => $model)
                <tr>
                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1)}}</td>
                    <td>
                        TRX-{{ $model->id }} <br />
                        <small class="text-muted">{{ date('d M Y H:i:s', strtotime($model->created_at))}}</small>
                    </td>
                    <td>
                        <div class=""><strong>{{ $model->member->nama_usaha }}</strong></div> 
                    </td>
                    <td>Rp. {{ number_format($model->total,2,',','.') }}</td>
                    <td>
                        @if($model->status == 0)
                        <label class="label label-default">Menunggu Pembayaran</label>
                        @elseif($model->status == 5)
                        <label class="label label-warning">Menunggu Konfirmasi</label><br />
                        <small>
                            Dari Bank : {{ $model->konfirmasi_dari_bank }} <br />
                            Ke Bank : {{ $model->konfirmasi_ke_bank }}<br />
                            Tgl Bayar : {{ date('d M Y', strtotime($model->tgl_bayar)) }}
                        </small>
                        @elseif($model->status == 10)
                        <label class="label label-success">Sukses</label><br />
                        <small>
                            Dari Bank : {{ $model->konfirmasi_dari_bank }} <br />
                            Ke Bank : {{ $model->konfirmasi_ke_bank }}<br />
                            Tgl Bayar : {{ date('d M Y', strtotime($model->tgl_bayar)) }}
                        </small>
                        @endif
                    </td>
                    <td class="text-right">
                        @if($model->status != 10)
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('admin.transaksi.update', ['id' => $model->id]) }}" data-method="PUT" data-confirm="Apakah anda yakin ingin mengkonfirmasi transaksi ini ?" data-toggle="tooltip" title="" class="btn btn-warning" data-original-title="Konfirmasi">
                                <i class="fa fa-check"></i>
                            </a>
                        </div>
                        @endif
                    </td>
                </tr>
                @endforeach

                @if($models->count() == 0)
                <tr>
                    <td colspan="6">
                        <i>Tidak ada data untuk ditampilkan</i>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        {!! $models->appends(['search' => Request::get('search')])->links() !!}
    </div>
</div>
@endsection