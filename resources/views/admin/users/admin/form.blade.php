@extends('layouts.dashboard.admin')

@section('title',$model->exists ? 'Ubah Admin' : 'Tambah Admin')
@section('page-title',$model->exists ? 'Ubah Admin' : 'Tambah Admin')

@section('content')
<div class="block">
    <form action="{{ $model->exists ? route('admin.users.admin.update', ['id' => $model->id]) : route('admin.users.admin.store') }}" method="POST" class="form-horizontal form-bordered">
        {{         csrf_field() }}
        {{ $model->exists ? method_field('PUT') : method_field('POST') }}
        <div class="form-group">
            <label class="col-md-3 control-label">Nama</label>
            <div class="col-md-9">
                <input type="text" name="nama" class="form-control" value="{{ old('nama', $model->nama) }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">
                <input type="text" name="email" class="form-control" value="{{ old('email', $model->email) }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Password</label>
            <div class="col-md-9">
                <input type="password" name="password" class="form-control" value="">
                @if($model->exists)
                <span class="help-block">Kosongkan password jika tidak ingin diganti</span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Status</label>
            <div class="col-md-9">
                <select name="status" class="form-control">
                    <option value="0" {{ old('status', $model->status) == 0 ? 'selected' : null }} >Tidak aktif</option>
                    <option value="1" {{ old('status', $model->status) == 1 ? 'selected' : null }}>Aktif</option>
                </select>
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-sm btn-primary">
                    <i class="fa fa-angle-right"></i> 
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>


@endsection