@extends('layouts.dashboard.admin')

@section('title',$model->exists ? 'Ubah Customer' : 'Tambah Customer')
@section('page-title',$model->exists ? 'Ubah Customer' : 'Tambah Customer')

@section('content')
<div class="block">
    <form action="{{ $model->exists ? route('admin.users.customer.update', ['id' => $model->id]) : route('admin.users.customer.store') }}" method="POST" class="form-horizontal form-bordered">
        {{         csrf_field() }}
        {{ $model->exists ? method_field('PUT') : method_field('POST') }}
        <div class="form-group">
            <label class="col-md-3 control-label">Nama</label>
            <div class="col-md-9">
                <input type="text" name="nama" class="form-control" value="{{ old('nama', $model->nama) }}" placeholder="Nama">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Email</label>
            <div class="col-md-9">
                <input type="text" name="email" class="form-control" value="{{ old('email', $model->email) }}" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Password</label>
            <div class="col-md-9">
                <input type="password" name="password" class="form-control" value="">
                @if($model->exists)
                <span class="help-block">Kosongkan password jika tidak ingin diganti</span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">No Telp.</label>
            <div class="col-md-9">
                <input type="number" name="no_telp" value="{{old('no_telp', $model->no_telp) }}" class=" form-control" placeholder="No Telp.">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Alamat</label>
            <div class="col-md-9">
                <input type="text" class=" form-control" value="{{old('alamat', $model->alamat) }}"  name="alamat" placeholder="Alamat">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Tipe identitas</label>
            <div class="col-md-9">
                <select name="tipe_id" class="form-control">
                    <option value="KTP" {{ old('tipe_id', $model->tipe_id) == 'KTP' ? 'selected' : null }}>KTP</option>
                    <option value="SIM" {{ old('tipe_id', $model->tipe_id) == 'SIM' ? 'selected' : null }} >SIM</option>
                    <option value="PASPORT" {{ old('tipe_id', $model->tipe_id) == 'PASPORT' ? 'selected' : null }}>PASPORT</option>
                    <option value="KITAS" {{ old('tipe_id', $model->tipe_id) == 'KITAS' ? 'selected' : null }}>KITAS</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">No Identitas</label>
            <div class="col-md-9">
                <input type="text" class=" form-control" name="no_id" placeholder="No Identitas" value="{{old('no_id', $model->no_id) }}" >
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Status</label>
            <div class="col-md-9">
                <select name="status" class="form-control">
                    <option value="0" {{ old('status', $model->status) == 0 ? 'selected' : null }} >Tidak aktif</option>
                    <option value="1" {{ old('status', $model->status) == 1 ? 'selected' : null }}>Aktif</option>
                </select>
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-sm btn-primary">
                    <i class="fa fa-angle-right"></i> 
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>


@endsection