@extends('layouts.dashboard.admin')

@section('title',$model->exists ? 'Ubah Member' : 'Tambah Member')
@section('page-title',$model->exists ? 'Ubah Member' : 'Tambah Member')

@section('content')
<form action="{{ $model->exists ? route('admin.users.member.update', ['id' => $model->id]) : route('admin.users.member.store') }}" method="POST" class="">
    {{         csrf_field() }}
    {{ $model->exists ? method_field('PUT') : method_field('POST') }}

    <div class="row">
        <div class="col-md-8"><div class="block">
                <div class="form-group">
                    <label class="control-label">Nama Pemilik</label>
                    <input type="text" name="nama_pemilik" class="form-control" value="{{ old('nama_pemilik', $model->nama_pemilik) }}">
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <input type="text" name="email" class="form-control" value="{{ old('email', $model->email) }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Password</label>
                    <input type="password" name="password" class="form-control" value="">
                    @if($model->exists)
                    <span class="help-block">Kosongkan password jika tidak ingin diganti</span>
                    @endif
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label">Nama Usaha</label>
                    <input type="text" name="nama_usaha" class="form-control" value="{{ old('nama_usaha', $model->nama_usaha) }}">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">No Telp</label>
                            <input type="text" name="no_telp" class="form-control" value="{{ old('no_telp', $model->no_telp) }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Kode Pos</label>
                            <input type="text" name="kode_pos" class="form-control" value="{{ old('kode_pos', $model->kode_pos) }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Alamat</label>
                    <textarea name="alamat" class="form-control">{{ old('alamat', $model->alamat) }}</textarea>
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control">{{ old('deskripsi', $model->deskripsi) }}</textarea>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="block">

                <div class="form-group">
                    <label class="control-label">Status</label>
                    <select name="status" class="form-control">
                        <option value="0" {{ old('status', $model->status) == 0 ? 'selected' : null }} >Tidak aktif</option>
                        <option value="1" {{ old('status', $model->status) == 1 ? 'selected' : null }}>Aktif</option>
                    </select>
                </div>
            </div>
            
            <div class="block">
                <div class="form-group">
                    <label class="control-label">Layanan</label>
                    <select name="id_layanan" class="form-control">
                        <option value="">Pilih Layanan</option>
                        @foreach(\App\Layanan::all() as $layanan)
                        <option 
                            {{ $model->id_layanan == $layanan->id ? 'selected' : null }}
                            value="{{ $layanan->id }}">{{ $layanan->nama }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="block">
                <div class="form-group">
                    <label class="control-label">Tanggal Tenggang</label>
                    <input type="text" name="tgl_tenggang" data-date-format="yyyy-mm-dd" class="form-control input-datepicker-close" value="{{ old('tgl_tenggang', $model->tgl_tenggang) }}">
                </div>
            </div>

            <div class="text-left">
                <button type="submit" class="btn btn-primary"> 
                    Submit
                </button>
            </div>
        </div>
    </div>
</form>


@endsection