@extends('layouts.dashboard.admin')

@section('title', $model->exists ? 'Ubah Wilayah' : 'Tambah Wilayah')
@section('page-title',$model->exists ? 'Ubah Wilayah' : 'Tambah Wilayah')


@section('content')
<div class="block">
    <form action="{{ $model->exists ? route('admin.wilayah.update', ['id' => $model->id]) : route('admin.wilayah.store') }}" method="POST" class="form-horizontal form-bordered">
        {{         csrf_field() }}
        {{ $model->exists ? method_field('PUT') : method_field('POST') }}
        <div class="form-group">
            <label class="col-md-3 control-label">Nama Wilayah</label>
            <div class="col-md-9">
                <input type="text" name="nama" class="form-control" value="{{ old('nama', $model->nama) }}">
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-sm btn-primary">
                    <i class="fa fa-angle-right"></i> 
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>


@endsection