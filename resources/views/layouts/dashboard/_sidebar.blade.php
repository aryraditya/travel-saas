<div id="sidebar">
    <!-- Wrapper for scrolling functionality -->
    <div class="sidebar-scroll">
        <!-- Sidebar Content -->
        <div class="sidebar-content">
            <!-- Brand -->
            <a href="#" class="sidebar-brand">
                <img src="{{ url('img/yuto-white-500x.png') }}" style="width: 150px" />
            </a>
            <!-- END Brand -->
            
            @includeIf('layouts.dashboard.menu.'. @$sidebar)
        </div>
        <!-- END Sidebar Content -->
    </div>
    <!-- END Wrapper for scrolling functionality -->
</div>