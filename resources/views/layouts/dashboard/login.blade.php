@extends('layouts.dashboard.main')

@section('layouts')
<img src="{{ url('dist/dashboard/img/placeholders/backgrounds/login_full_bg.jpg') }}" alt="Login Full Background" class="full-bg animation-pulseSlow">
<div id="login-container" class="animation-fadeIn">
    @yield('content')
</div>
@endsection