<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>@yield('title')</title>

        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0"/>
        <meta name="csrf-token" content="{{ csrf_token() }}" />

     <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="img/favicon.ico">

     <!-- Stylesheets -->
        <!-- Bootstrap is included in its original form, unaltered -->
        <link rel="stylesheet" href="{{ url('dist/dashboard/css/bootstrap.min.css') }}" />

     <!-- Related styles of various icon packs and plugins -->
        <link rel="stylesheet" href="{{ url('dist/dashboard/css/plugins.css') }}" />

     <!-- The main stylesheet of this template. All Bootstrap overwrites are defined in here -->
        <link rel="stylesheet" href="{{ url('dist/dashboard/css/main.css') }}" />

     <!-- Include a specific file here from css/themes/ folder to alter the default theme of the template -->

     <!-- The themes stylesheet of this template (for using specific theme color in individual elements - must included last) -->
        <link rel="stylesheet" href="{{ url('dist/dashboard/css/themes.css') }}">
        <!-- END Stylesheets -->

     <!-- Modernizr (browser feature detection library) & Respond.js (Enable responsive CSS code on browsers that don't support it, eg IE8) -->
        <script src="{{ url('dist/dashboard/js/vendor/modernizr-2.7.1-respond-1.4.2.min.js') }}"></script>
        
        @stack('head')
    </head>

    <body>

        @yield('layouts')

        <!-- Scroll to top link, initialized in js/app.js - scrollToTop() -->
        <a href="#" id="to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- Include Jquery library from Google's CDN but if something goes wrong get Jquery from local file (Remove 'http:' if you have SSL) -->
        <script src="{{ url('dist/dashboard/js/vendor/jquery-1.11.1.min.js') }}"></script>

        <!-- Bootstrap.js, Jquery plugins and Custom JS code -->
        <script src="{{ url('dist/dashboard/js/jquery.maskMoney.min.js') }}"></script>
        <script src="{{ url('dist/dashboard/js/vendor/bootstrap.min.js') }}"></script>
        <script src="{{ url('dist/dashboard/js/plugins.js') }}"></script>
        <script src="{{ url('dist/dashboard/js/app.js') }}"></script>
        <script type="text/javascript">
            $('.mask-money').maskMoney({
                thousands: '.',
                decimal:',',
            }).maskMoney('mask');
        </script>
        @stack('scripts')
    </body>
</html>