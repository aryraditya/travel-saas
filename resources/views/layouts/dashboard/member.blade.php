@extends('layouts.dashboard.main')

@section('layouts')
<div id="page-container" class="sidebar-partial sidebar-visible-lg sidebar-no-animations">
    <!-- Main Sidebar -->
    @include('layouts.dashboard._sidebar', ['sidebar' => 'member'])
    <!-- END Main Sidebar -->

    <!-- Main Container -->
    <div id="main-container">
        <!-- Header -->
        <header class="navbar navbar-default">
            <ul class="nav navbar-nav-custom">
                <li>
                    <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');">
                        <i class="fa fa-bars fa-fw"></i>
                    </a>
                </li>
            </ul>
            <!-- END Left Header Navigation -->

            <!-- Right Header Navigation -->
            <ul class="nav navbar-nav-custom pull-right">

                <!-- User Dropdown -->
                <li class="dropdown">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ url('dist/dashboard/img/placeholders/avatars/avatar2.jpg') }}" alt="avatar"> <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                        <li class="dropdown-header text-center">Account</li>
                        <li>
                            <!-- Opens the user settings modal that can be found at the bottom of each page (page_footer.html in PHP version) -->
                            <a href="{{ route('member.account') }}">
                                <i class="fa fa-cog fa-fw pull-right"></i>
                                Settings
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="{{ route('member.auth.logout') }}"><i class="fa fa-ban fa-fw pull-right"></i> Logout</a>
                        </li>
                    </ul>
                </li>
                <!-- END User Dropdown -->
            </ul>
            <!-- END Right Header Navigation -->
        </header>
        <!-- END Header -->

        <!-- Page content -->
        <div id="page-content">
            <!-- Blank Header -->
            <div class="content-header">
                <div class="header-section">
                    <h1>
                        @yield('title')
                    </h1>
                </div>
            </div>

            @if(Session::has('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="fa fa-check-circle"></i> Sukses</h4> {{ Session::pull('success') }}
            </div>
            @endif

            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif

            @yield('content')
        </div>
        <!-- END Page Content -->

    </div>
    <!-- END Main Container -->
</div>
<!-- END Page Container -->
@endsection