<!-- Sidebar Navigation -->
<ul class="sidebar-nav">
    <li>
        <a href="{{ route('admin.dashboard') }}"><i class="gi gi-stopwatch sidebar-nav-icon"></i>Dashboard</a>
    </li>
    <li class="divider"></li>
    <li>
        <a href="{{ route('admin.transaksi.index') }}"><i class="gi gi-money sidebar-nav-icon"></i>Transaksi Member</a>
    </li>
    <li class="divider"></li>
    <li>
        <a href="{{ route('admin.layanan.index') }}"><i class="gi gi-adjust sidebar-nav-icon"></i>Layanan</a>
    </li>
    <li class="sidebar-header">
        <span class="sidebar-header-title">Wilayah</span>
    </li>
    <li>
        <a href="{{ route('admin.wilayah.index') }}"><i class="gi gi-globe sidebar-nav-icon"></i>Daftar Wilayah</a>
    </li>
    <li>
        <a href="{{ route('admin.wilayah.create') }}"><i class="gi gi-plus sidebar-nav-icon"></i>Tambah Wilayah</a>
    </li>
    <li class="sidebar-header">
        <span class="sidebar-header-title">Users</span>
    </li>
    <li>
        <a href="{{ route('admin.users.admin.index') }}"><i class="gi gi-lock sidebar-nav-icon"></i>Administrator</a>
    </li>
    <li>
        <a href="{{ route('admin.users.member.index') }}"><i class="gi gi-user_add sidebar-nav-icon"></i>Member</a>
    </li>
    <li>
        <a href="{{ route('admin.users.customer.index') }}"><i class="gi gi-user sidebar-nav-icon"></i>Customer</a>
    </li>
</ul>
<!-- END Sidebar Navigation -->