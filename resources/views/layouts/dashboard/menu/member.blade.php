<div class="sidebar-section sidebar-user clearfix sidebar-nav-mini-hide" style="padding-left:0px; text-align:center">
    <div style="font-size:22px;">
        {{ Auth::guard('member')->user()->layanan->nama }}
    </div>
    <div>
        <small>Berlaku sampai</small> <br />
        {{ Auth::guard('member')->user()->tgl_tenggang }}
    </div>

</div>
<!-- Sidebar Navigation -->
<ul class="sidebar-nav">
    <li>
        <a href="{{ route('member.dashboard') }}"><i class="gi gi-stopwatch sidebar-nav-icon"></i>Dashboard</a>
    </li>
    <li class="divider"></li>
    <li>
        <a href="{{ route('member.pemesanan.index') }}"><i class="gi gi-book_open sidebar-nav-icon"></i>Pemesanan</a>
    </li>
    <li class="sidebar-header">
        <span class="sidebar-header-title">Tour</span>
    </li>
    <li>
        <a href="{{ route('member.tour.index') }}"><i class="gi gi-bicycle sidebar-nav-icon"></i>Daftar Tour</a>
    </li>
    <li>
        <a href="{{ route('member.tour.create') }}"><i class="gi gi-plus sidebar-nav-icon"></i>Tambah Tour</a>
    </li>
    <li class="sidebar-header">
        <span class="sidebar-header-title">Bank</span>
    </li>
    <li>
        <a href="{{ route('member.bank.index') }}"><i class="gi gi-bank sidebar-nav-icon"></i>Daftar Bank</a>
    </li>
    <li>
        <a href="{{ route('member.bank.create') }}"><i class="gi gi-plus sidebar-nav-icon"></i>Tambah Bank</a>
    </li>
</ul>
<!-- END Sidebar Navigation -->