@extends('layouts.dashboard.main')

@section('layouts')
<div class="container">
    <div class="row">
        <div class="col-md-5 col-md-offset-1">
            <div id="login-alt-container" style="top:50px;">
                <!-- Title -->
                <h1 class="push-top-bottom">
                    <img src="{{ url('img/yuto-white-500x.png')}}" width="300" />
                    <br />
                    <small style="line-height: 25px">Daftar menjadi member travel dari yuto.id, nikmati kemudahan menjual paket tour anda!</small>
                </h1>
                <!-- END Title -->

                <!-- Key Features -->
                <ul class="fa-ul text-muted" style="font-size:15px">
                    <li><i class="fa fa-check fa-li text-success"></i> Jutaan Visitor</li>
                    <li><i class="fa fa-check fa-li text-success"></i> Meningkatkan pemensanan tour Anda</li>
                    <li><i class="fa fa-check fa-li text-success"></i> Bebas biaya transaksi</li>
                    <li><i class="fa fa-check fa-li text-success"></i> Pemesanan langsung menuju rekening Anda</li>
                </ul>
                <!-- END Key Features -->
            </div>
        </div>
        <div class="col-md-5">
            <!-- Login Container -->
            <div id="login-container" style="top:50px;">
                @yield('content')
            </div>
            <!-- END Login Container -->
        </div>
    </div>
</div>
@endsection