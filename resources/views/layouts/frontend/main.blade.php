
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Yuto - Yuk Tour Kemana Aja</title>

        <!-- CSS -->
        <link href="{{ url('dist/frontend/css/base.css') }}" rel="stylesheet">

        <!-- CSS -->
        <link href="{{ url('dist/frontend/css/slider-pro.min.css') }}" rel="stylesheet">
        <link href="{{ url('dist/frontend/css/date_time_picker.css') }}" rel="stylesheet">

        <!-- Google web fonts -->
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Gochi+Hand' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>

        <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>

        <!--[if lte IE 8]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a>.</p>
        <![endif]-->
        <!--
                <div id="preloader">
                    <div class="sk-spinner sk-spinner-wave">
                        <div class="sk-rect1"></div>
                        <div class="sk-rect2"></div>
                        <div class="sk-rect3"></div>
                        <div class="sk-rect4"></div>
                        <div class="sk-rect5"></div>
                    </div>
                </div>-->
        <!-- End Preload -->

        <div class="layer"></div>
        <!-- Mobile menu overlay mask -->

        <!-- Header================================================== -->
        <header id="plain" class="">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div id="logo">
                            <a href="{{ route('home') }}"><img src="{{ url('img/yuto-original-500x.png') }}" style="margin-left:-20px" height="34" alt="City tours" data-retina="true" class="logo_normal"></a>
                            <a href="{{ route('home') }}"><img src="{{ url('img/yuto-original-500x.png') }}" style="margin-left:-20px"height="34" alt="City tours" data-retina="true" class="logo_sticky"></a>
                        </div>
                    </div>
                    <nav class="col-md-9 col-sm-9 col-xs-9">
                        <a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
                        <div class="main-menu">
                            <div id="header_menu">
                                <img src="{{ url('img/yuto-original-500x.png') }}" width="160" height="34" alt="City tours" data-retina="true">
                            </div>
                            <a href="#" class="open_close" id="close_in">
                                <i class="icon_set_1_icon-77"></i>
                            </a>
                            <ul class="pull-right-md">
                                <li>
                                    <a href="{{ route('home') }}">Home</a>
                                </li>
                                @if(Auth::guest())
                                <li>
                                    <a href="{{ route('register') }}">Register</a>
                                </li>
                                <li>
                                    <a href="{{ route('login') }}">Login</a>
                                </li>
                                @else
                                <li class="submenu">
                                    <a href="javascript:void(0);" class="show-submenu">{{ Auth::user()->nama }} <i class="icon-down-open-mini"></i></a><ul>
                                        <li>
                                            <a href="{{ route('mybooking') }}">Pemesanan Saya</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('mysetting') }}">Profil</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('logout') }}">Logout</a>
                                        </li>
                                    </ul>
                                </li>
                                @endif
                                <li>
                                    <a href="{{ route('member.auth.register') }}">
                                        <span  style=" padding: 5px; color:#fff; background:#e04f67">Bergabung menjadi member</span>
                                    </a>
                                </li>
                            </ul>
                        </div><!-- End main-menu -->
                    </nav>
                </div>
            </div><!-- container -->
        </header><!-- End Header -->

        @yield('content')

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div style="text-align: center">
                            <p>© PT. Yuk Tour Kemana Aja {{ date('Y') }}</p>
                        </div>
                    </div>
                </div><!-- End row -->
            </div><!-- End container -->
        </footer><!-- End footer -->

        <div id="toTop"></div><!-- Back to top button --> 
        <div id="overlay"></div><!-- Mask on input focus -->  

        <!-- Common scripts -->
        <script src="{{ url('dist/frontend/js/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ url('dist/frontend/js/common_scripts_min.js') }}"></script>
        <script src="{{ url('dist/frontend/js/bootstrap-datepicker.js') }}"></script>
        <script src="{{ url('dist/frontend/js/wNumb.js') }}"></script>
        <script src="{{ url('dist/frontend/js/countdown.js') }}"></script>
        <script src="{{ url('dist/frontend/js/functions.js') }}"></script>

        <script>
                            //Search bar
                            $(function () {
                                "use strict";
                                $("#searchDropdownBox").change(function () {
                                    changeSearchDropDownBox();
                                });
                                
                                changeSearchDropDownBox();
                                
                                function changeSearchDropDownBox(){
                                    var Search_Str = $('#searchDropdownBox').find(':selected').text();
                                    //replace search str in span value
                                    $("#nav-search-in-content").text(Search_Str);
                                }
                            });
        </script>
        @stack('scripts')
    </body>
</html>