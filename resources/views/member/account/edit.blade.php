@extends('layouts.dashboard.member')

@section('title', 'Ubah data akun')
@section('page-title', 'Ubah data akun')

@section('content')
<form action="{{ route('member.account') }}" method="POST" class="">
    {{         csrf_field() }}

    <div class="row">
        <div class="col-md-8"><div class="block">
                <div class="form-group">
                    <label class="control-label">Nama Pemilik</label>
                    <input type="text" name="nama_pemilik" class="form-control" value="{{ old('nama_pemilik', $model->nama_pemilik) }}">
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <input type="text" name="email" class="form-control" value="{{ old('email', $model->email) }}">
                </div>
                <div class="form-group">
                    <label class="control-label">Password</label>
                    <input type="password" name="password" class="form-control" value="">
                    @if($model->exists)
                    <span class="help-block">Kosongkan password jika tidak ingin diganti</span>
                    @endif
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label">Nama Usaha</label>
                    <input type="text" name="nama_usaha" class="form-control" value="{{ old('nama_usaha', $model->nama_usaha) }}">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">No Telp</label>
                            <input type="text" name="no_telp" class="form-control" value="{{ old('no_telp', $model->no_telp) }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Kode Pos</label>
                            <input type="text" name="kode_pos" class="form-control" value="{{ old('kode_pos', $model->kode_pos) }}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label">Alamat</label>
                    <textarea name="alamat" class="form-control">{{ old('alamat', $model->alamat) }}</textarea>
                </div>
                <hr />
                <div class="form-group">
                    <label class="control-label">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control">{{ old('deskripsi', $model->deskripsi) }}</textarea>
                </div>
            </div>

            <div class="text-right mb40">
                <button type="submit" class="btn btn-primary"> 
                    Submit
                </button>
            </div>
        </div>
    </div>
</form>


@endsection