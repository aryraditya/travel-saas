@extends('layouts.dashboard.member')

@section('title', 'Dashboard')

@section('content')
<form action="{{ route('member.upgrade') }}" method="POST">
    {{ csrf_field() }}
    <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><i class="gi gi-coins"></i></span>
                <select name="id_layanan" class="form-control input-lg">
                    @foreach(\App\Layanan::all() as $layanan)
                    <option 
                        value="{{ $layanan->id }}">{{ $layanan->nama }} - Rp. {{ number_format($layanan->harga,2,',','.') }} / {{ $layanan->durasi}} Bulan</option>
                    @endforeach
                </select>
            </div>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-block btn-success">Upgrade</button>
</div>
</form>
@endsection