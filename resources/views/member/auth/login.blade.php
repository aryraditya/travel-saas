@extends('layouts.dashboard.login')

@section('content')
<!-- Login Title -->
<div class="login-title text-center">
    <h1><i class="gi gi-lock"></i> <strong>Member</strong></h1>
</div>
<!-- END Login Title -->

<!-- Login Block -->
<div class="block push-bit">
    @if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <!-- Login Form -->
    <form action="{{ route('member.auth.login')}} " method="post" id="form-login" class="form-horizontal form-bordered form-control-borderless" novalidate="novalidate">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                    <input type="text" id="login-email" name="email" class="form-control input-lg" placeholder="Email">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-asterisk"></i></span>
                    <input type="password" id="login-password" name="password" class="form-control input-lg" placeholder="Password">
                </div>
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-xs-4">
                <label class="switch switch-primary" data-toggle="tooltip" title="" data-original-title="Remember Me?">
                    <input type="checkbox" id="login-remember-me" name="remember" value="1">
                    <span></span>
                </label>
            </div>
            <div class="col-xs-8 text-right">
                <button type="submit" class="btn btn-sm btn-primary"> Login to Dashboard</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 text-center">
                <a href="{{ route('member.auth.register') }}" id="link-register-login"><small>Daftar menjadi member yuto</small></a>
            </div>
        </div>
    </form>
    <!-- END Login Form -->
</div>
<!-- END Login Block -->
@endsection