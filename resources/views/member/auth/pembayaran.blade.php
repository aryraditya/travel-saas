@extends('layouts.dashboard.register')

@section('content')

<!-- Login Title -->
<div class="login-title text-center">
    <h1><strong>Register</strong></h1>
</div>
<!-- END Login Title -->

<div class="block">
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="fa fa-check-circle"></i> Sukses</h4> {{ Session::pull('success') }}
    </div>
    @endif
    <p>
        Terimakasih telah melakukan pendaftaran, Berikut adalah data akun Anda :
    </p>

    <dl class="dl-horizontal">
        <dt>ID Transaksi</dt>
        <dd>TRX-{{ $model->id }}</dd>
        <dt>Nama Usaha</dt>
        <dd>{{ $model->member->nama_usaha }}</dd>
        <dt>Nama Pemilik</dt>
        <dd>{{ $model->member->nama_pemilik }}</dd>
        <dt>Email</dt>
        <dd>{{ $model->member->email }}</dd>
        <dt>Password</dt>
        <dd>********</dd>
    </dl>

    <p>
        Silahkan melakukan pembayaran layanan ke salah satu bank berikut :
    </p>

    <div class="row">
        <div class="col-md-6">
            <div class="block">
                <div class="block-title ui-sortable-handle">
                    <h2>BCA</h2>
                </div>
                <p>
                    <strong>093222833822</strong>  <br>
                    PT. Yuk Tour Kemana Aja<br>
                    <strong>KCP </strong> Denpasar
                </p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="block">
                <div class="block-title ui-sortable-handle">
                    <h2>Mandiri</h2>
                </div>
                <p>
                    <strong>0932272882</strong>  <br>
                    PT. Yuk Tour Kemana Aja<br>
                    <strong>KCP </strong> Denpasar
                </p>
            </div>
        </div>
    </div>


    <p>
        Jika Anda sudah melakukan pembayaran silahkan melakukan konfirmasi pembayaran dengan menekan tombol dibawah :
    </p>

    <div class="text-center mb25">
        <button class="btn btn-success" data-toggle="modal" data-target="#modal-konfirmasi">
            Konfirmasi Pembayaran
        </button>
    </div>

    <div id="modal-konfirmasi" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <form method="POST" action="{{ route('member.auth.pembayaran', ['id' => $model->id]) }}">

                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Konfirmasi Pembayaran</h4>
                    </div>
                    <div class="modal-body">
                        @if($model->tgl_bayar)
                        <div class="alert alert-warning">
                            Anda sudah melakukan konfirmasi pembayaran
                        </div>
                        @endif
                        {{csrf_field()}}
                        {{method_field('POST')}}

                        <div class="form-group">
                            <label>Bank Anda</label>
                            <input type="text" required name="from_bank" class="form-control" value="{{ old('from_bank', @explode('-',$model->konfirmasi_dari_bank)[0]) }}">
                        </div>
                        <div class="form-group">
                            <label>No. Rekening Anda</label>
                            <input type="text" required name="from_no" class="form-control" value="{{ old('from_no', @explode('-',$model->konfirmasi_dari_bank)[1]) }}">
                        </div>
                        <div class="form-group">
                            <label>Ke Bank</label>
                            <select name="to_bank" class="form-control">
                                <option value="BCA" @if($model->konfirmasi_ke_bank == 'BCA') selected @endif>BCA</option>
                                <option value="Mandiri" @if($model->konfirmasi_ke_bank == 'Mandiri') selected @endif>Mandiri</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <br />
    <div class="form-group clearfix">
        <div class="col-xs-12 text-center">
            <a href="{{ route('member.auth.login') }}" id="link-register"><small>Kembali ke halaman login</small></a>
        </div>
    </div>
</div>

@endsection

@push('head')
<style>
    .modal-backdrop{
        z-index: 0;
    }
</style>
@endpush