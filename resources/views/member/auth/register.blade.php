@extends('layouts.dashboard.register');

@section('content')
<!-- Login Title -->
<div class="login-title text-center">
    <h1><strong>Daftar Member</strong></h1>
</div>
<!-- END Login Title -->

<!-- Login Block -->
<div class="block push-bit">

    <!-- Register Form -->
    <form action="{{ route('member.auth.register') }}" method="post" id="form-register" class="form-horizontal display-none" novalidate="novalidate" style="display: block;">
        {{ csrf_field() }}
        {{ method_field('POST') }}

        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-user"></i></span>
                    <input type="text" name="nama_pemilik" class="form-control input-lg" value="{{ old('nama_pemilik', $model->nama_pemilik) }}" placeholder="Nama Pemilik">
                </div>
            </div>
        </div>
        <hr />
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-envelope"></i></span>
                    <input type="text" name="email" class="form-control input-lg" value="{{ old('email', $model->email) }}" placeholder="Email">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-lock"></i></span>
                    <input type="password" name="password" class="form-control input-lg" value="" placeholder="Password">
                </div>
            </div>
        </div>
        <hr />
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-home"></i></span>
                    <input type="text" name="nama_usaha" class="form-control input-lg" placeholder="Nama Usaha" value="{{ old('nama_usaha', $model->nama_usaha) }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-phone_alt"></i></span>
                    <input type="text" name="no_telp" class="form-control input-lg" placeholder="No. Telp" value="{{ old('no_telp', $model->no_telp) }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-vcard"></i></span>
                    <input type="text" name="kode_pos" class="form-control input-lg" placeholder="Kode POS" value="{{ old('kode_pos', $model->kode_pos) }}">
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-vcard"></i></span>
                    <textarea name="alamat" class="form-control input-lg" placeholder="Alamat">{{ old('alamat', $model->alamat) }}</textarea>
                </div>
            </div>
        </div>
        <hr/>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="input-group">
                    <span class="input-group-addon"><i class="gi gi-coins"></i></span>
                    <select name="id_layanan" class="form-control input-lg">
                        <option value="">Pilih Layanan</option>
                        @foreach(\App\Layanan::all() as $layanan)
                        <option 
                            {{ $model->id_layanan == $layanan->id ? 'selected' : null }}
                            value="{{ $layanan->id }}">{{ $layanan->nama }} - Rp. {{ number_format($layanan->harga,2,',','.') }} / {{ $layanan->durasi}} Bulan</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <hr />
        <div class="form-group form-actions">
            <div class="col-xs-6">
                <!--                                <a href="#modal-terms" data-toggle="modal" class="register-terms">Terms</a>
<label class="switch switch-primary" data-toggle="tooltip" title="" data-original-title="Agree to the terms">
<input type="checkbox" id="register-terms" name="register-terms">
<span></span>
</label>-->
            </div>
            <div class="col-xs-6 text-right">
                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> Register Account</button>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12 text-center">
                <small>Sudah punya akun ?</small> <a href="{{ route('member.auth.login') }}" id="link-register"><small>Login disini</small></a>
            </div>
        </div>
    </form>
    <!-- END Register Form -->
</div>

@endsection