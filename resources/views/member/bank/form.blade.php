@extends('layouts.dashboard.member')

@section('title',$model->exists ? 'Ubah Bank' : 'Tambah Bank')
@section('page-title',$model->exists ? 'Ubah Bank' : 'Tambah Bank')

@section('content')
<div class="block">
    <form action="{{ $model->exists ? route('member.bank.update', ['id' => $model->id]) : route('member.bank.store') }}" method="POST" class="form-horizontal form-bordered">
        {{         csrf_field() }}
        {{ $model->exists ? method_field('PUT') : method_field('POST') }}
        <div class="form-group">
            <label class="col-md-3 control-label">Nama Bank</label>
            <div class="col-md-9">
                <input type="text" name="nama" class="form-control" value="{{ old('nama', $model->nama_bank) }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">No Rek.</label>
            <div class="col-md-9">
                <input type="text" name="no" class="form-control" value="{{ old('no', $model->no_rek) }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Rek. Atas Nama</label>
            <div class="col-md-9">
                <input type="text" name="atas_nama" class="form-control" value="{{ old('atas_nama', $model->atas_nama) }}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">KCP</label>
            <div class="col-md-9">
                <input type="text" name="kcp" class="form-control" value="{{ old('kcp', $model->kcp) }}">
            </div>
        </div>
        <div class="form-group form-actions">
            <div class="col-md-9 col-md-offset-3">
                <button type="submit" class="btn btn-sm btn-primary">
                    <i class="fa fa-angle-right"></i> 
                    Submit
                </button>
            </div>
        </div>
    </form>
</div>


@endsection