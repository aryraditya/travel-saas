@extends('layouts.dashboard.member')

@section('title', 'Daftar Bank')
@section('page-title', 'Daftar Bank')

@section('content')
<div class="block">
    <div class="row mb15">
        <div class="col-md-6">
            <a href="{{ route('member.bank.create') }}" class="btn btn-warning">
                <i class="gi gi-plus"></i>
                Tambah Bank
            </a>
        </div>
        <div class="col-md-6">
            <form class="form-inline" action="{{ route('member.bank.index') }}">
                <div  class="dataTables_filter">
                    <label>
                        <div class="input-group">
                            <input type="search" name="search" value="{{ Request::get('search') }}" class="form-control" aria-controls="example-datatable" placeholder="Cari...">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </label>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Bank</th>
                    <th>No Rek.</th>
                    <th>Atas Nama</th>
                    <th>KCP</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($models as $key => $model)
                <tr>
                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1)}}</td>
                    <td>
                        <div class=""><strong>{{ $model->nama_bank }}</strong></div> 
                    </td>
                    <td>{{ $model->no_rek }}</td>
                    <td>{{ $model->atas_nama }}</td>
                    <td>{{ $model->kcp }}</td>
                    
                    <td class="text-right">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('member.bank.edit', ['id' => $model->id]) }}" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                            <a href="{{ route('member.bank.destroy', ['id' => $model->id]) }}" data-toggle="tooltip" title="" data-method="DELETE" data-confirm="Apakah anda yakin ingin menghapus bank ini ?" class="btn btn-danger" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach

                @if($models->count() == 0)
                <tr>
                    <td colspan="6">
                        <i>Tidak ada data untuk ditampilkan</i>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        {!! $models->appends(['search' => Request::get('search')])->links() !!}
    </div>
</div>
@endsection