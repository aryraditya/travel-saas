@extends('layouts.dashboard.member')

@section('title', 'Dashboard')

@section('content')

@if(Session::has('inactive'))
<div class="alert alert-danger">
    <i class="gi gi-warning_sign"></i>
    Akun anda telah memasuki masa tenggang, silahkan <a href='{{ route('member.upgrade') }}'>Upgrade</a> akun anda!
</div>
@endif
<div class="row">
    <div class="col-sm-6">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background-fire">
                    <i class="gi gi-warning_sign"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                        {{ \App\Pesanan::byMember()->where(['status' => 5])->count() }}
                        <strong>Pesanan</strong>
                    <small><em>Menunggu konfirmasi</em></small>
                </h4>
            </div>
        </div>
    </div>
    
    <div class="col-sm-6">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background-fire">
                    <i class="gi gi-pause"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                        {{ \App\Pesanan::byMember()->where(['status' => 0])->count() }}
                        <strong>Pesanan</strong>
                    <small><em>Menunggu pembayaran</em></small>
                </h4>
            </div>
        </div>
    </div>
    
    
    <div class="col-sm-6 col-md-4">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background">
                    <i class="gi gi-book_open"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                    <a href="#">
                        {{ \App\Pesanan::totalPesanan(\Auth::guard('member')->id(), date('n')) }}
                        <strong>Pesanan</strong>
                    </a>
                    <small><em>Bulan {{ date('F') }}</em></small>
                </h4>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background">
                    <i class="gi gi-book_open"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                    <a href="#">
                        {{ \App\Pesanan::totalPesanan(\Auth::guard('member')->id(), null, date('Y')) }} 
                        <strong>Pesanan</strong>
                    </a>
                    <small><em>Tahun {{date('Y')}}</em></small>
                </h4>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background">
                    <i class="gi gi-book_open"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                    <a href="#">
                        {{ \App\Pesanan::totalPesanan(\Auth::guard('member')->id()) }} 
                        <strong>Pesanan</strong>
                    </a>
                    <small><em>Total</em></small>
                </h4>
            </div>
        </div>
    </div>


    <div class="col-sm-6 col-md-4">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background-autumn">
                    <i class="gi gi-usd"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                    <a href="#">
                        Rp. 
                        <strong>{{  number_format(\App\Pesanan::totalPendapatan(\Auth::guard('member')->id(), date('n')), 2, ',', '.') }} </strong>
                    </a>
                    <small><em>Pendapatan Bulan {{ date('F') }}</em></small>
                </h4>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background-autumn">
                    <i class="gi gi-usd"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                    <a href="#">
                        Rp. 
                        <strong>{{ number_format(\App\Pesanan::totalPendapatan(\Auth::guard('member')->id(), null, date('Y')), 2, ',', '.') }} </strong>
                    </a>
                    <small><em>Pendapatan Tahun {{ date('Y') }}</em></small>
                </h4>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-md-4">
        <div class="widget">
            <div class="widget-simple">
                <a href="javascript:void(0)" class="widget-icon pull-left animation-fadeIn themed-background-autumn">
                    <i class="gi gi-usd"></i>
                </a>
                <h4 class="widget-content text-right animation-hatch">
                    <a href="#">
                        Rp. 
                        <strong>{{ number_format(\App\Pesanan::totalPendapatan(\Auth::guard('member')->id()), 2, ',', '.') }} </strong>
                    </a>
                    <small><em>Total Pendapatan</em></small>
                </h4>
            </div>
        </div>
    </div>
</div>

<div class="widget">
    <div class="widget-advanced widget-advanced-alt">
        <!-- Widget Header -->
        <div class="widget-header text-center themed-background">
            <h3 class="widget-content-light text-left pull-left animation-pullDown">
                <strong>Pesanan & Pendapatan</strong><br>
                <small>{{date('Y')}}</small>
            </h3>

            <div id="chart-widget1" class="chart"></div>
        </div>
        <!-- END Widget Header -->
    </div>
</div>

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        // Get the elements where we will attach the charts
        var chartWidget1 = $('#chart-widget1');
        var chartWidget2 = $('#chart-widget2');

        // Random data for the charts
        var dataEarnings = {{ json_encode(\App\Pesanan::arrayTotalPendapatan(\Auth::guard('member')->id(), date('Y'))) }};
        var dataSales = {{ json_encode(\App\Pesanan::arrayTotalPesanan(\Auth::guard('member')->id(), date('Y'))) }};

        // Array with month labels used in both charts
        var chartMonths = [[1, 'January'], [2, 'February'], [3, 'March'], [4, 'April'], [5, 'May'], [6, 'June'], [7, 'July'], [8, 'August'], [9, 'September'], [10, 'October'], [11, 'November'], [12, 'December']];

        // Widget 1 Chart
        $.plot(chartWidget1, [
            {
                data: dataEarnings,
                lines: {show: true, fill: false},
                points: {show: true, radius: 6, fillColor: '#cccccc'}
            },
            {
                data: dataSales,
                lines: {show: true, fill: false},
                points: {show: true, radius: 6, fillColor: '#ffffff'}
            }
        ], {
            colors: ['#ffffff', '#353535'],
            legend: {show: false},
            grid: {borderWidth: 0, hoverable: true, clickable: true},
            yaxis: {show: false},
            xaxis: {show: false, ticks: chartMonths}
        });


        // Creating and attaching a tooltip to both charts
        var previousPoint = null, ttlabel = null;
        chartWidget1.bind('plothover', function (event, pos, item) {
            if (item) {
                if (previousPoint !== item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $('#chart-tooltip').remove();
                    var x = item.datapoint[0], y = item.datapoint[1];

                    // Get xaxis label
                    var monthLabel = item.series.xaxis.options.ticks[item.dataIndex][1];

                    if (item.seriesIndex === 1) {
                        ttlabel = '<strong>' + y + '</strong> pemesanan di bulan <strong>' + monthLabel + '</strong>';
                    } else {
                        ttlabel = 'Rp. <strong>' + y + '</strong> pendapatan di bulan <strong>' + monthLabel + '</strong>';
                    }

                    $('<div id="chart-tooltip" class="chart-tooltip">' + ttlabel + '</div>')
                            .css({top: item.pageY - 50, left: item.pageX - 50}).appendTo("body").show();
                }
            } else {
                $('#chart-tooltip').remove();
                previousPoint = null;
            }
        });
    });
</script>
@endpush