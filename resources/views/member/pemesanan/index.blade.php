@extends('layouts.dashboard.member')

@section('title', 'Daftar Pemesanan')
@section('page-title', 'Daftar Pemesanan')

@section('content')
<div class="block">
    <form class="form-inline" action="{{ route('member.pemesanan.index') }}">
        <div class="row mb15">
            <div class="col-md-4">
                <div class="input-group input-daterange" data-date-format="yyyy-mm-dd">
                    <input type="text" id="example-daterange1" name="from" class="form-control text-center" placeholder="Dari" value="{{ Request::get('from') }}">
                    <span class="input-group-addon"><i class="fa fa-angle-right"></i></span>
                    <input type="text" id="example-daterange2" name="to" class="form-control text-center" placeholder="Sampai" value="{{ Request::get('to') }}">
                </div>
            </div>
            <div class="col-md-3">
                <select name="status" class="form-control" style="width:100%">
                    <option value="-10" @if(\Request::get('status') == -10) selected @endif>Semua Status</option>
                    <option value="0" @if(\Request::get('status') === '0') selected @endif>Menunggu pembayaran</option>
                    <option value="5" @if(\Request::get('status') == 5) selected @endif>Menunggu konfirmasi</option>
                    <option value="10" @if(\Request::get('status') == 10) selected @endif>Diterima</option>
                </select>
            </div>
            <div class="col-md-2">
                <label>
                    <div class="input-group">
                        <input type="search" name="search" value="{{ Request::get('search') }}" class="form-control" aria-controls="example-datatable" placeholder="Cari...">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </label>
            </div>
            <div class="col-md-3 text-right">
                <a href="{{ route('member.pemesanan.print',[
                    'search' => Request::get('search'),
                    'from'  => Request::get('from'),
                    'to'    => Request::get('to'),
                    'status' => Request::get('status'),
                ]) }}" target="_blank" class="btn btn-info">
                    <i class="gi gi-print"></i> Export PDF
                </a>
            </div>
        </div>
    </form>

    <div class="table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>ID</th>
                    <th>Nama Customer</th>
                    <th>Tour</th>
                    <th>Total</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($models as $key => $model)
                <tr>
                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1)}}</td>
                    <td>RSV-{{ $model->id }} <br>
                        <small class="text-muted">{{date('d M Y', strtotime($model->created_at))}}</small>
                    </td>
                    <td>
                        <div class=""><strong>{{ $model->cust_nama }}</strong></div> 
                        <div class="text-muted">{{ $model->cust_email }}</div> 
                    </td>
                    <td>{{ $model->tour->nama }} <br> <small class="text-muted"> Tanggal : {{ date('d F Y', strtotime($model->tgl_tour)) }}</small></td>
                    <td>
                        Rp. {{ number_format($model->total, 2,',','.') }} <br />
                        <small class="text-info"><strong>{{ $model->total_pax }} pax</strong></small>
                    </td>
                    <td>
                        @if($model->status == 0)
                        <label class="label label-default">Menunggu pembayaran</label>
                        @elseif($model->status == 5)
                        <label class="label label-warning">Menunggu konfirmasi</label>
                        @elseif($model->status == 10)
                        <label class="label label-success">Diterima</label>
                        @elseif($model->status == -1)
                        <label class="label label-danger">Dibatalkan</label>
                        @endif
                    </td>
                    <td class="text-left">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('member.pemesanan.show', ['id' => $model->id])}}" data-toggle="tooltip" data-original-title="Lihat detail" class="btn btn-default">
                                <i class="fa fa-eye"></i>
                            </a>
                            @if($model->status == 0 || $model->status == 5)
                            <a href="{{ route('member.pemesanan.update', ['id' => $model->id]) }}" data-toggle="tooltip" title="" data-method="PUT" data-confirm="Apakah anda yakin ingin men-konfirmasi pemesanan ini ?" class="btn btn-success" data-original-title="Konfirmasi pemesanan">
                                <i class="fa fa-check"></i>
                            </a>
                            <a href="{{ route('member.pemesanan.update', ['id' => $model->id, 'action' => 'cancel']) }}" data-toggle="tooltip" title="" data-method="PUT" data-confirm="Apakah anda yakin ingin membatalkan pemesanan ini ?" class="btn btn-danger" data-original-title="Batalkan pemesanan">
                                <i class="fa fa-times"></i>
                            </a>
                            @elseif($model->status == 10)

                            @endif
                        </div>
                    </td>
                </tr>
                @endforeach

                @if($models->count() == 0)
                <tr>
                    <td colspan="6">
                        <i>Tidak ada data untuk ditampilkan</i>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        {!! $models->appends([
        'search' => Request::get('search'),
        'from'  => Request::get('from'),
        'to'    => Request::get('to'),
        'status' => Request::get('status'),
        ])->links() !!}
    </div>
</div>
@endsection