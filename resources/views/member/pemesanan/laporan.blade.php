<!DOCTYPE html>
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <style>
            @page {
                header: page-header;
                footer: page-footer;
            }
            .table {
                width: 100%;
                max-width: 100%;
                margin-bottom: 1rem;
                border-collapse: collapse;
            }

            .table th,
            .table td {
                padding: 0.75rem;
                vertical-align: top;
                border-top: 1px solid #eceeef;
            }

            .table thead th {
                vertical-align: bottom;
                border-bottom: 2px solid #eceeef;
                text-align:left;
            }

            .table tbody + tbody {
                border-top: 2px solid #eceeef;
            }

            .table .table {
                background-color: #fff;
            }

            .table-sm th,
            .table-sm td {
                padding: 0.3rem;
            }

            .table-bordered {
                border: 1px solid #eceeef;
            }

            .table-bordered th,
            .table-bordered td {
                border: 1px solid #eceeef;
            }

            .table-bordered thead th,
            .table-bordered thead td {
                border-bottom-width: 2px;
            }

            .table-striped tbody tr:nth-of-type(odd) {
                background-color: rgba(0, 0, 0, 0.05);
            }

            .table-hover tbody tr:hover {
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table-active,
            .table-active > th,
            .table-active > td {
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table-hover .table-active:hover {
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table-hover .table-active:hover > td,
            .table-hover .table-active:hover > th {
                background-color: rgba(0, 0, 0, 0.075);
            }

            .table-success,
            .table-success > th,
            .table-success > td {
                background-color: #dff0d8;
            }

            .table-hover .table-success:hover {
                background-color: #d0e9c6;
            }

            .table-hover .table-success:hover > td,
            .table-hover .table-success:hover > th {
                background-color: #d0e9c6;
            }

            .table-info,
            .table-info > th,
            .table-info > td {
                background-color: #d9edf7;
            }

            .table-hover .table-info:hover {
                background-color: #c4e3f3;
            }

            .table-hover .table-info:hover > td,
            .table-hover .table-info:hover > th {
                background-color: #c4e3f3;
            }

            .table-warning,
            .table-warning > th,
            .table-warning > td {
                background-color: #fcf8e3;
            }

            .table-hover .table-warning:hover {
                background-color: #faf2cc;
            }

            .table-hover .table-warning:hover > td,
            .table-hover .table-warning:hover > th {
                background-color: #faf2cc;
            }

            .table-danger,
            .table-danger > th,
            .table-danger > td {
                background-color: #f2dede;
            }

            .table-hover .table-danger:hover {
                background-color: #ebcccc;
            }

            .table-hover .table-danger:hover > td,
            .table-hover .table-danger:hover > th {
                background-color: #ebcccc;
            }

            .thead-inverse th {
                color: #fff;
                background-color: #292b2c;
            }

            .thead-default th {
                color: #464a4c;
                background-color: #eceeef;
            }

            .table-inverse {
                color: #fff;
                background-color: #292b2c;
            }

            .table-inverse th,
            .table-inverse td,
            .table-inverse thead th {
                border-color: #fff;
            }
        </style>
    </head>

    <body>
        
    <htmlpageheader name="page-header">
        <div class="header" style="text-align:center; padding-top:30px">
            <table style="border-collapse: collapse; border-bottom:1px solid #eaeaea" width="100%">
                <tr>
                    <td width="30%">
                        <br />
                        <h2 style="margin-bottom:0px;padding-top:20px">Laporan Pemesanan</h2>
                        <div class="">Periode {{ date('d F Y', strtotime($from)) }} - {{ date('d F Y', strtotime($to)) }}</div>
                    </td>
                    <td width="40%" align="center">
                        <h1 style="margin:0 0 15px 0; padding:0">{{ Auth::guard('member')->user()->nama_usaha }}</h1>
                        <br>
                        <div style="color:#999;">{{ Auth::guard('member')->user()->alamat }}</div>
                    </td>
                    <td width="30%" align="right">
                        Tanggal cetak : <br />{{ date('d F Y H:i:s') }} <br /> <br />
                        Hal. {PAGENO} dari {nb}
                    </td>
                </tr>
                <tr>
                    <td colspan="3" height="25"></td>
                </tr>
            </table>
        </div>
    </htmlpageheader>
        
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Customer</th>
                    <th>Tour</th>
                    <th>Total</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($models as $key => $model)
                <tr>
                    <td>RSV-{{ $model->id }} <br>
                        <small class="text-muted">{{date('d M Y', strtotime($model->created_at))}}</small>
                    </td>
                    <td>
                        <div class=""><strong>{{ $model->cust_nama }}</strong></div> 
                        <div class="text-muted">{{ $model->cust_email }}</div> 
                    </td>
                    <td>{{ $model->tour->nama }} <br> <small class="text-muted"> Tanggal : {{ date('d F Y', strtotime($model->tgl_tour)) }}</small></td>
                    <td>
                        Rp. {{ number_format($model->total, 2,',','.') }} <br />
                        <small class="text-info"><strong>{{ $model->total_pax }} pax</strong></small>
                    </td>
                    <td>
                        @if($model->status == 0)
                        <label class="label label-default">Menunggu pembayaran</label>
                        @elseif($model->status == 5)
                        <label class="label label-warning">Menunggu konfirmasi</label>
                        @elseif($model->status == 10)
                        <label class="label label-success">Diterima</label>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
</body>
</html>