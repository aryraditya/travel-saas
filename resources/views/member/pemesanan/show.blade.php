@extends('layouts.dashboard.member')

@section('title', 'Pesanan #'.$model->id)
@section('page-title', 'Pesanan #'.$model->id)

@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="block">
            <div class="block-title">
                <h2>Data pemesan</h2>
            </div>
            
            <h4><strong>{{ $model->cust_nama }}</strong></h4>
            <address>
                {{$model->cust_alamat}} <br /> <br/>
                {{ $model->cust_tipe_id }} - {{ $model->cust_no_id }} <br/> <br />
                
                <i class="fa fa-phone"></i> {{ $model->cust_no_telp}}<br>
                <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)">{{ $model->cust_email }}</a>
            </address>
        </div>
        <div class="block">
            <div class="block-title">
                <h2>Data tour</h2>
            </div>
            
            <h4><strong>{{ $model->tour->nama }}</strong></h4>
            <div><i class="fa fa-map-marker"></i> {{ $model->tour->getWilayah->nama }} </div>
            <dl class="dl-horizontal mt25">
                <dt style="text-align: left">Tanggal Tour</dt>
                <dd>{{ date('d F Y', strtotime($model->tgl_tour)) }}</dd>
                <dt style="text-align: left">Total Pax</dt>
                <dd>{{ $model->total_pax }} Orang</dd>
                <dt style="text-align: left">Tanggal Pemesanan</dt>
                <dd>{{ date('d F Y', strtotime($model->created_at)) }}</dd>
                <dt style="text-align: left">Total Pembayaran</dt>
                <dd>Rp. {{ number_format($model->total, 2,',','.') }}</dd>
            </dl>
        </div>
    </div>
    <div class="col-md-4">
        @if($model->status == 0)
        <div class="btn btn-default btn-lg btn-block">
            Menungg pembayaran
        </div>
        @elseif($model->status == 5)
        <div class="btn btn-warning btn-lg btn-block">
            Menungg konfirmasi
        </div>
        @elseif($model->status == 10)
        <div class="btn btn-info btn-lg btn-block">
            Dikonfirmasi
        </div>
        @endif
        
        
        @if($model->status == 0 || $model->status == 5)
        <a href="{{ route('member.pemesanan.update', ['id' => $model->id]) }}" data-toggle="tooltip" title="" data-method="PUT" data-confirm="Apakah anda yakin ingin men-konfirmasi pemesanan ini ?" class="btn btn-success btn-block" data-original-title="Konfirmasi pemesanan">
            Konfirmasi Pesanan
        </a>
        @endif
        
        
        <div class="block mt25">
            <div class="block-title">
                <h2>Data Pembayaran</h2>
            </div>
            <dl class=" mt25">
                <dt style="text-align: left">Tanggal Pembayaran</dt>
                <dd>{{ date('d F Y', strtotime($model->tgl_bayar)) }}</dd>
                
                <dt style="text-align: left; margin-top:10px">Bank Customer</dt>
                <dd>{{ $model->konfirmasi_dari_bank }}</dd>
                
                <dt style="text-align: left; margin-top:10px">Ke Bank</dt>
                <dd>{{ $model->konfirmasi_ke_bank }}</dd>
                
                <dt style="text-align: left; margin-top:10px">Total Pembayaran</dt>
                <dd>Rp. {{ number_format($model->total, 2,',','.') }}</dd>
            </dl>
        </div>
    </div>
</div>

@endsection