@extends('layouts.dashboard.member')

@section('title', $model->exists ? 'Ubah Paket Tour' : 'Tambah Paket Tour')
@section('page-title',$model->exists ? 'Ubah Paket Tour' : 'Tambah Paket Tour')


@section('content')
<form action="{{ $model->exists ? route('member.tour.update', ['id' => $model->id]) : route('member.tour.store') }}" method="POST" class="" enctype='multipart/form-data'>
    <div class="row">
        <div class="col-md-8">
            <div class="block">
                {{ csrf_field() }}
                {{ $model->exists ? method_field('PUT') : method_field('POST') }}
                <div class="form-group required">
                    <label class="control-label">Nama Tour</label>
                    <input type="text" name="nama" class="form-control" value="{{ old('nama', $model->nama) }}">
                </div>
                <div class="form-group required">
                    <label class="control-label">Wilayah</label>
                    <select name="wilayah" class="form-control">
                        <option value="">Pilih Wilayah</option>
                        @foreach(\App\Wilayah::orderBy('nama','asc')->get() as $wilayah)
                        <option value="{{ $wilayah->id }}" @if($wilayah->id == old('wilayah', $model->wilayah)) selected @endif>{{$wilayah->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group required">
                    <label class="control-label">Harga</label>
                    <input type="text" name="harga" class="form-control mask-money" value="{{ old('harga', $model->harga) }}">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label">Min. Pax</label>
                            <input type="number" min="1" name="min_pax" class="form-control" value="{{ old('min_pax', $model->min_pax) }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group required">
                            <label class="control-label">Max. Pax</label>
                            <input type="number" name="max_pax" class="form-control" value="{{ old('max_pax', $model->max_pax) }}">
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label">Deskripsi</label>
                    <textarea name="deskripsi" class="form-control ckeditor">{{ old('deskripsi', $model->deskripsi) }}</textarea>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="block">
                <div class="form-group">
                    <label class="control-label">Gambar 1</label>
                    @if($model->gambar_1)
                    <div>
                        <img src="{{ $model->getImagePath($model->gambar_1) }}" width="100" />
                    </div>
                    @endif
                    <input type="file" name="gambar_1" class="form-control" />
                </div>
                <div class="form-group">
                    <label class="control-label">Gambar 2</label>
                    @if($model->gambar_2)
                    <div>
                        <img src="{{ $model->getImagePath($model->gambar_2) }}" width="100" />
                    </div>
                    @endif
                    <input type="file" name="gambar_2" class="form-control" />
                </div>
                <div class="form-group">
                    <label class="control-label">Gambar 3</label>
                    @if($model->gambar_3)
                    <div>
                        <img src="{{ $model->getImagePath($model->gambar_3) }}" width="100" />
                    </div>
                    @endif
                    <input type="file" name="gambar_3" class="form-control" />
                </div>
            </div>
            <div class="block">
                <div class="form-group">
                    <label class="control-label">Status</label>
                    <select name="status" class="form-control">
                        <option value="1">Aktif</option>
                        <option value="0">Tidak aktif</option>
                    </select>
                </div>
            </div>
            <div class="form-group form-actions">
                <button type="submit" class="btn btn-sm btn-primary">
                    <i class="fa fa-angle-right"></i> 
                    Submit
                </button>
            </div>
        </div>
    </div>
</form>


@endsection

@push('scripts')
<script type="text/javascript" src="{{ url('dist/dashboard/js/ckeditor/ckeditor.js') }}"></script>

<script type="text/javascript">
CKEDITOR.replace('deskripsi', {
    height: 500
});
</script>
@endpush