@extends('layouts.dashboard.member')

@section('title', 'Daftar Paket Tour')
@section('page-title', 'Daftar Paket Tour')

@section('content')
<div class="block">
    <div class="row mb15">
        <div class="col-md-6">
            <a href="{{ route('member.tour.create') }}" class="btn btn-warning">
                <i class="gi gi-plus"></i>
                Tambah Paket Tour
            </a>
        </div>
        <div class="col-md-6">
            <form class="form-inline" action="{{ route('member.tour.index') }}">
                <div  class="dataTables_filter">
                    <label>
                        <div class="input-group">
                            <input type="search" name="search" value="{{ Request::get('search') }}" class="form-control" aria-controls="example-datatable" placeholder="Cari...">
                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                        </div>
                    </label>
                </div>
            </form>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover table-striped table-condensed">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Nama Paket Tour</th>
                    <th>Wilayah</th>
                    <th>Harga</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach($models as $key => $model)
                <tr>
                    <td>{{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1)}}</td>
                    <td>
                        <div class=""><strong>{{ $model->nama }}</strong></div> 
                    </td>
                    <td>{{ $model->getWilayah->nama }}</td>
                    <td>Rp. {{ number_format($model->harga, 2,',','.') }}</td>
                    <td>
                        @if($model->status)
                        <label class="label label-success">Aktif</label>
                        @else
                        <label class="label label-danger">Tidak Aktif</label>
                        @endif
                    </td>
                    <td class="text-right">
                        <div class="btn-group btn-group-xs">
                            <a href="{{ route('member.tour.edit', ['id' => $model->id]) }}" data-toggle="tooltip" title="" class="btn btn-default" data-original-title="Edit"><i class="fa fa-pencil"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach

                @if($models->count() == 0)
                <tr>
                    <td colspan="6">
                        <i>Tidak ada data untuk ditampilkan</i>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>
        {!! $models->appends(['search' => Request::get('search')])->links() !!}
    </div>
</div>
@endsection