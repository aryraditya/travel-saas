@extends('layouts.frontend.main')

@section('content')
<section id="hero">
    <div class="intro_title">
        <h3 class="animated fadeInDown">Yu<span style="font-weight: normal; color: #fff">k</span> To<span style="font-weight: normal; color: #fff">ur Kemana Aja</span></h3>
        <p class="animated fadeInDown">Disini tournya lengkap...</p>
        <!--<a href="#" class="animated fadeInUp button_intro">View Tours</a> <a href="#" class="animated fadeInUp button_intro outline">View Tickets</a>-->
    </div>

    <div id="search_bar_container">
        <div class="container">
            <div class="search_bar">
                <form action="{{ route('search') }}" method="get">
                    <span class="nav-facade-active" id="nav-search-in">
                        <span id="nav-search-in-content" style="">All tours</span>
                        <span class="nav-down-arrow nav-sprite"></span>
                        <select title="Search in" class="searchSelect" id="searchDropdownBox" name="w">
                            <option value="0" title="Semua wilayah">Semua wilayah</option>
                            @foreach($wilayah as $wil)
                            <option value="{{ $wil->id }}" title="{{ $wil->nama }}">{{ $wil->nama }}</option>
                            @endforeach
                        </select>
                    </span>
                    <div class="nav-searchfield-outer">
                        <input type="text" autocomplete="off" name="q" placeholder="Cari tour..." id="twotabsearchtextbox">
                    </div>
                    <div class="nav-submit-button">
                        <input type="submit" title="Cerca" class="nav-submit-input" value="">
                    </div>
                </form>
            </div><!-- End search bar-->
        </div>
    </div><!-- /search_bar-->
</section>


<section>
    <div class="container margin_60">
        <div class="main_title">
            <h2><span>Populer</span> Tour</h2>
        </div>
        <div class="row">
            @foreach($tours as $tour)
            <div class="col-md-4 col-sm-6">
                <div class="tour_container">
                    <div class="img_container">
                        <a href="{{ route('tour', ['id' => $tour->id])}}">
                            <img src="{{ url( $tour->getImagePath($tour->getImage()))}}" class="img-responsive" alt="Image">
                            <div class="short_info">
                                <i class="icon_set_1_icon-41"></i>{{ $tour->getWilayah->nama }}<span class="price"><sup>Rp.</sup>{{ number_format($tour->harga, 2,',','.')}}</span>
                            </div>
                        </a>
                    </div>
                    <div class="tour_title">
                        <h3  style="height:40px; overflow: hidden; margin-bottom:10px" title='{{$tour->nama}}'><strong>{{ $tour->nama }}</strong></h3>
                        <div class="rating">
                            <i class="icon_set_1_icon-29"></i> {{ $tour->owner->nama_usaha}}
                        </div>
                    </div>
                </div><!-- End box tour -->
            </div><!-- End col-md-4 -->

            @endforeach

        </div>
    </div><!--End container -->
</section>

<section class="white_bg">
    <div class="container margin_60">
        <div class="main_title">
            <h2><span>Destinasi</span> Tour</h2>
        </div>

        <div class="row add_bottom_45">
            @foreach($wilayah as $key => $wil)

            @if($key == 0 || $key % 5 == 0)
            <div class="col-md-4 other_tours">
                <ul>
                    @endif
                    <li>
                        <a href="{{ route('search', ['w' => $wil->id])}}">
                            <i class="icon_set_1_icon-37"></i>{{ $wil->nama }}
                            <!--<span class="other_tours_price">$42</span>-->
                        </a>
                    </li>
                    @if($key % 5 == 4 || $key == 14 || count($wilayah) == $key)
                </ul>
            </div>
            @endif

            <?php
            if ($key == 14) {
                break;
            }
            ?>
            @endforeach
        </div>


    </div><!-- End container -->
</section>
@endsection