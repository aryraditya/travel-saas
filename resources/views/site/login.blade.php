@extends('layouts.frontend.main')

@section('content')
<section id="hero" class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div id="login">
                    <div class="text-center"><img src="{{ url('img/yuto-original-500x.png') }}" alt="Image" data-retina="complete" class="img-responsive"></div>
                    <hr>
                    
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('login') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" value="{{ Request::get('url') }}" name="url" />
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class=" form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class=" form-control" id="password1" placeholder="Password">
                        </div>
                        <div id="pass-info" class="clearfix"></div>
                        <button class="btn_full">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection