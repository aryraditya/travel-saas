@extends('layouts.frontend.main')

@section('content')
<div class="container margin_60" style="margin-top:30px">
    <div class="main_title">
        <h2><span>Pesanan</span> Saya</h2>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-3">
            <div class="box_style_cat">
                <ul id="cat_nav">
                    <li>
                        <a href="{{ route('mybooking') }}" id="active">
                            <i class="icon_set_1_icon-51"></i>
                            Pesanan Saya
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('mysetting') }}">
                            <i class="icon_set_1_icon-29"></i>
                            Profil Saya
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-8 col-lg-9">
            <div class="panel-group" id="accordion">
                @foreach($models as $model)
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_{{ $model->id }}" aria-expanded="false">
                                {{ $model->tour->nama }}
                                <i class="indicator pull-right icon-plus"></i>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse_{{$model->id}}" class="panel-collapse collapse" aria-expanded="false">
                        <div class="panel-body">
                            <table class="table confirm">
                                <tbody>
                                    <tr>
                                        <td>
                                            <strong>STATUS</strong>
                                        </td>
                                        <td>
                                            @if($model->status == 0)
                                            <span class="text-warning">Anda belum melakukan konfirmasi pembayaran</span> <a href="{{ route('bayar', ['id' => $model->id]) }}">Klik disini untuk melakukan konfirmasi pembayaran</a>
                                            @elseif($model->status == 5)
                                            <span class="text-info">Menunggu Konfirmasi Dari Merchant</span>
                                            @elseif($model->status == -1)
                                            <span class="text-danger">Pesanan dibatalkan</span>
                                            @else
                                            <span class="text-success">Pesanan Diterima oleh merchant</span>
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>ID Pesanan</strong>
                                        </td>
                                        <td>
                                            RSV-{{ $model->id }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Total Pax</strong>
                                        </td>
                                        <td>
                                            {{ $model->total_pax }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Tanggal Tour</strong>
                                        </td>
                                        <td>
                                            {{ $model->tgl_tour }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Total Pembayaran</strong>
                                        </td>
                                        <td>
                                            Rp. {{ number_format($model->total,2 ,',','.') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <h4>Detail Pemesan</h4>
                            <table class="table confirm">
                                <tbody>
                                    <tr>
                                        <td>
                                            <strong>Nama</strong>
                                        </td>
                                        <td>
                                            {{ $model->cust_nama }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Email</strong>
                                        </td>
                                        <td>
                                            {{ $model->cust_email }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Alamat</strong>
                                        </td>
                                        <td>
                                            {{ $model->cust_alamat }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>Identitas</strong>
                                        </td>
                                        <td>
                                            {{ $model->cust_tipe_id }} - {{ $model->cust_no_id }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection