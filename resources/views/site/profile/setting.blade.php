@extends('layouts.frontend.main')

@section('content')
<div class="container margin_60" style="margin-top:30px">
    <div class="main_title">
        <h2><span>Profil</span> Saya</h2>
    </div>
    <div class="row">
        <div class="col-md-4 col-lg-3">
            <div class="box_style_cat">
                <ul id="cat_nav">
                    <li>
                        <a href="{{ route('mybooking') }}">
                            <i class="icon_set_1_icon-51"></i>
                            Pesanan Saya
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('mysetting') }}" id="active">
                            <i class="icon_set_1_icon-29"></i>
                            Profil Saya
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="col-md-8 col-lg-9">
            @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('save_profile') }}" method="POST">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Nama</label>
                    <input type="text" name="nama" class=" form-control" placeholder="Nama" value="{{ old('nama', $model->nama) }}">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input type="email"  name="email" class=" form-control" placeholder="Email" value="{{ old('email', $model->email) }}">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" name="password" class=" form-control" id="password1" placeholder="Password">
                    <div class="help-block text-muted">Kosongkan jika tidak ingin mengganti password</div>
                </div>
                <div class="form-group">
                    <label>No Telp.</label>
                    <input type="number" name="no_telp" class=" form-control" placeholder="No Telp." value="{{ old('no_telp', $model->no_telp) }}">
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class=" form-control" name="alamat" placeholder="Alamat" value="{{ old('alamat', $model->alamat) }}">
                </div>
                <div class="form-group">
                    <label>Tipe identitas</label>
                    <select name="tipe_id" class="form-control">
                        <option value="KTP" @if($model->tipe_id=='KTP') selected @endif>KTP</option>
                        <option value="SIM" @if($model->tipe_id=='SIM') selected @endif>SIM</option>
                        <option value="PASPORT" @if($model->tipe_id=='PASPORT') selected @endif>PASPORT</option>
                        <option value="KITAS" @if($model->tipe_id=='KITAS') selected @endif>KITAS</option>
                    </select>
                </div>
                <div class="form-group">
                    <label>No Identitas</label>
                    <input type="text" class=" form-control" name="no_id" placeholder="No Identitas" value="{{ old('no_id', $model->no_id) }}">
                </div>
                <div id="pass-info" class="clearfix"></div>
                <button class="btn_full">Update Akun</button>
            </form>
        </div>
    </div>
</div>
@endsection