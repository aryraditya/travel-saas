@extends('layouts.frontend.main')

@section('content')
<section id="hero" class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div id="login">
                    <div class="text-center"><img src="{{ url('img/yuto-original-500x.png') }}" alt="Image" data-retina="complete" class="img-responsive"></div>
                    <hr>

                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form action="{{ route('register') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Nama</label>
                            <input type="text" name="nama" class=" form-control" placeholder="Nama">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email"  name="email" class=" form-control" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="password" class=" form-control" id="password1" placeholder="Password">
                        </div>
                        <div class="form-group">
                            <label>No Telp.</label>
                            <input type="number" name="no_telp" class=" form-control" placeholder="No Telp.">
                        </div>

                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class=" form-control" name="alamat" placeholder="Alamat">
                        </div>
                        <div class="form-group">
                            <label>Tipe identitas</label>
                            <select name="tipe_id" class="form-control">
                                <option value="KTP">KTP</option>
                                <option value="SIM">SIM</option>
                                <option value="PASPORT">PASPORT</option>
                                <option value="KITAS">KITAS</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>No Identitas</label>
                            <input type="text" class=" form-control" name="no_id" placeholder="No Identitas">
                        </div>
                        <div id="pass-info" class="clearfix"></div>
                        <button class="btn_full">Buat akun</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection