@extends('layouts.frontend.main')

@section('content')
<div class="container margin_60" style="margin-top:30px">
    <div class="row">
        <div class="col-md-8 add_bottom_15">

            @if($model->tgl_bayar)
            <div class="alert alert-success">
                Terimakasih, Anda sudah melakukan konfirmasi pembayaran, silahkan tunggu pihak travel untuk mengkonfirmasi pembayaran Anda.
            </div>
            @endif

            <div class="form_title">
                <h3><strong><i class="icon-ok"></i></strong>Terimakasih! </h3>
                <p>
                    {{ $model->cust_nama }}.
                </p>
            </div>
            <div class="step">
                <p>
                    <strong>Terimakasih {{ $model->cust_nama }}</strong>, Anda telah melakukan pemesanan tour <strong>{{ $model->tour->nama }}</strong> pada tanggal {{ date('d F Y H:i:s', strtotime($model->created_at)) }}.

                </p>
                <p>
                    Silahkan melakukan pembayaran ke bank berikut :
                </p>
                <div class="row">
                    @foreach( $model->tour->owner->bank as $bank )
                    <div class="col-md-4">
                        <div class="box_style_4">
                            <h4><span> {{ $bank->nama_bank }} </span></h4>
                            <div style="font-size:14px"><strong>{{ $bank->no_rek }}</strong></div>
                            <div><small>a/n {{ $bank->atas_nama }}</small></div>
                            <div><small>KCP {{ $bank->kcp }}</small></div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div><!--End step -->

            <div class="form_title">
                <h3><strong><i class="icon-tag-1"></i></strong>Detail pesanan Anda</h3>
                <p>
                    Berikut adalah detail pesanan Anda.
                </p>
            </div>
            <div class="step">
                <table class="table confirm">
                    <thead>
                        <tr>
                            <th colspan="2">
                                {{ $model->tour->nama }}
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <strong>ID Pesanan</strong>
                            </td>
                            <td>
                                RSV-{{ $model->id }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Total Pax</strong>
                            </td>
                            <td>
                                {{ $model->total_pax }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Tanggal Tour</strong>
                            </td>
                            <td>
                                {{ date('d F Y', strtotime($model->tgl_tour)) }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Nama Pemesan</strong>
                            </td>
                            <td>
                                {{ $model->cust_nama }}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Total Pembayaran</strong>
                            </td>
                            <td>
                                Rp. {{ number_format($model->total, 2, ',', '.') }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!--End step -->
        </div><!--End col-md-8 -->

        <aside class="col-md-4">
            <div class="box_style_1">
                <h3 class="inner">Batas Waktu Pembayaran!</h3>
                @if($model->status != -1)
                <div class="countdown" style="font-size:30px;text-align:center;margin:50px 0 40px">
                    <span id="clock"></span>
                </div>
                <p>
                    Jika anda telah melakukan pembayaran, silahkan melakukan konfirmasi pembayaran dengan menekan tombol dibawah
                </p>
                <hr>
                <a class="btn_full_outline" href="#" data-toggle="modal" data-target="#modal-konfirmasi-bayar">Konfirmasi Pembayaran</a>
                @else
                <h6 class="text-danger text-center">
                    Pesanan anda telah dibatalkan karena melewati batas waktu pembayaran
                </h6>
                @endif
            </div>
            <div class="box_style_4">
                <i class="icon_set_1_icon-89"></i>
                <h4>Punya <span>Pertanyaan?</span></h4>
                <a href="#" class="phone">{{ $model->tour->owner->no_telp }}</a>
            </div>
        </aside>

    </div><!--End row -->
</div>

<div id="modal-konfirmasi-bayar" class="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form method="POST" action="{{ route('bayar', ['id' => $model->id]) }}">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Konfirmasi Pembayaran</h4>
                </div>
                <div class="modal-body">
                    @if($model->tgl_bayar)
                    <div class="alert alert-success">
                        Anda sudah melakukan konfirmasi pembayaran, silahkan tunggu pihak travel untuk mengkonfirmasi pembayaran Anda.
                    </div>
                    @endif

                    {{csrf_field()}}
                    {{method_field('POST')}}

                    <div class="form-group">
                        <label>Bank Anda</label>
                        <input type="text" required name="from_bank" class="form-control" value="{{ old('from_bank', @explode(' - ',$model->konfirmasi_dari_bank)[0]) }}">
                    </div>
                    <div class="form-group">
                        <label>No. Rekening Anda</label>
                        <input type="text" required name="from_no" class="form-control" value="{{ old('from_no', @explode(' - ',$model->konfirmasi_dari_bank)[1]) }}">
                    </div>
                    <div class="form-group">
                        <label>Ke Bank</label>
                        <select name="to_bank" class="form-control">
                            @foreach($model->tour->owner->bank as $bank)
                            <option value="{{ $bank->nama_bank }} - {{ $bank->no_rek }}">{{ $bank->nama_bank }} - {{ $bank->no_rek }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        $('#clock').countdown('{{ $batas }}', function(event) {
            $(this).html(event.strftime('%H:%M:%S'));
        });
    });
</script>
@endpush