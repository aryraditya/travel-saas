@extends('layouts.frontend.main')

@section('content')
<div class="container margin_60" style="margin-top:30px">
    <div class="row">
        <div class="col-md-8" id="single_tour_desc">
            <div id="single_tour_feat">
                <h1 style="border-bottom:1px solid #ccc; padding-bottom:15px; margin:0 0 10px">{{ $model->nama }}</h1>
            </div>

            <p class="visible-sm visible-xs"><a class="btn_map" data-toggle="collapse" href="#collapseMap" aria-expanded="false" aria-controls="collapseMap" data-text-swap="Hide map" data-text-original="View on map">View on map</a></p><!-- Map button for tablets/mobiles -->

            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    @foreach($model->getImage(true) as $key => $img)
                    <div class="item {{ $key == 0 ? 'active' : null }}">
                        <img src="{{ $model->getImagePath($img) }}">
                    </div>
                    @endforeach
                </div>

                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="icon icon-left-open" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="icon icon-right-open" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <hr>

            <div class="deskripsi">
                {!! $model->deskripsi !!}
            </div>
            <hr>
        </div><!--End  single_tour_desc-->

        <aside class="col-md-4">
            <form action="{{ route('pesan') }}" method="GET">
                <div class="box_style_1 expose">
                    <h3 class="inner">- Booking -</h3>

                    <div class="form-group">
                        <label><i class="icon-calendar-7"></i> Pilih tanggal</label>
                        <input class="date-pick form-control" data-date-format="yyyy-mm-dd" value="{{ date('Y-m-d') }}" name="tanggal" type="text">
                        <input type="hidden" name="tour_id" value="{{ $model->id }}">
                    </div>
                    <div class="form-group">
                        <label>Pax</label>
                        <input type="number" value="{{ $model->min_pax }}" min="{{ $model->min_pax }}" max="{{ $model->max_pax ? $model->max_pax : 20 }}" id="pax" class="form-control" name="pax">
                    </div>
                    <br>
                    <table class="table table_summary">
                        <tbody>
                            <tr>
                                <td>
                                    Pax
                                </td>
                                <td class="text-right pax_content">
                                    {{ $model->min_pax }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Harga/pax
                                </td>
                                <td class="text-right">
                                    <sup>Rp.</sup> {{ number_format($model->harga, 2,',','.') }}
                                </td>
                            </tr>
                            <tr class="total">
                                <td>
                                    Total
                                </td>
                                <td class="text-right">
                                    <sup>Rp</sup><span class="total_harga"></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="submit" class="btn_full">Book now</button>
                </div><!--/box_style_1 -->
            </form>

            <div class="box_style_4">
                <i class="icon_set_1_icon-90"></i>
                <h4><span>Pesan</span> dari telphone</h4>
                <a href="#" class="phone">{{ $model->owner->no_telp }}</a>
                <div>{{ $model->owner->nama_usaha }}</div>
            </div>

        </aside>
    </div><!--End row -->
</div>

@endsection

@push('scripts')
<script type="text/javascript">
$(document).ready(function(){
    var harga = {{ $model->harga }};
    calTotal();
    
    $('.date-pick').datepicker({
        'startDate' : '+1d'
    });
    
    $('#pax').on('change', function(){
        $('.pax_content').html($(this).val());
        calTotal();
    });
    
    function calTotal(){
        var moneyFormat = wNumb({
                mark: ',',
                thousand: '.',
        });
        var pax = $('#pax').val();
        var total = pax * harga;
        
        $('.total_harga').html(moneyFormat.to(total));
    }
});
</script>
@endpush