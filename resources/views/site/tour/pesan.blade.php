@extends('layouts.frontend.main')

@section('content')
<form action="{{ route('pesan') }}" method="POST">
    {{ csrf_field() }}
    <input type="hidden" name="tanggal_tour" value="{{ Request::get('tanggal') }}">
    <input type="hidden" name="total_pax" value="{{ Request::get('pax') }}">
    <input type="hidden" name="id_tour" value="{{ Request::get('tour_id') }}">
    <div class="container margin_60" style="margin-top:30px">
        <div class="row">
            <div class="col-md-8 add_bottom_15">
                <div class="form_title">
                    <h3><strong>1</strong>Data Diri Anda</h3>
                    <p>Masukkan data diri Pemesan</p>
                </div>
                <div class="step">
                    <div class="form-group">
                        <label>Nama Lengkap</label>
                        <input type="text" class="form-control" id="firstname_booking" name="nama" value="{{ old('nama', $model->cust_nama) }}">
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" id="email_booking" name="email" class="form-control" value="{{ old('email', $model->cust_email) }}">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>No Telp</label>
                                <input type="tel" id="email_booking_2" name="no_telp" class="form-control" value="{{ old('no_telp', $model->cust_no_telp) }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>Tipe identitas</label>
                                <select name="tipe_id" class="form-control">
                                    <option value="KTP">KTP</option>
                                    <option value="SIM">SIM</option>
                                    <option value="PASPORT">PASPORT</option>
                                    <option value="KITAS">KITAS</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="form-group">
                                <label>No Identitas</label>
                                <input type="no_id" name="no_id" class="form-control" value="{{ old('no_id', $model->cust_no_id) }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Alamat Lengkap</label>
                        <textarea class="form-control" name="alamat">{{ old('alamat', $model->cust_alamat) }}</textarea>
                    </div>
                </div><!--End step -->

                <div class="form_title">
                    <h3><strong>2</strong>Pembayaran</h3>
                    <p>
                        Pilih metode pembayaran.
                    </p>
                </div>
                <div class="step">
                    <div class="form-group">
                        <label>Tipe identitas</label>
                        <select name="metode_bayar" class="form-control">
                            <option value="transfer">Bank Transfer</option>
                        </select>
                    </div>
                </div><!--End step -->

                
                <div id="policy">
                    
                    <button type="submit" class="btn_1 green medium">Book now</button>
                </div>
            </div>

            <aside class="col-md-4">
                <div class="box_style_1">
                    <h3 class="inner">- Summary -</h3>
                    <div style="margin-bottom:20px; font-size:16px">
                        <strong>{{ $model->nama }} </strong>
                    </div>
                    <table class="table table_summary">
                        <tbody>
                            <tr>
                                <td>
                                    Tanggal Tour
                                </td>
                                <td class="text-right">
                                    {{ Request::get('tanggal') }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Total Pax
                                </td>
                                <td class="text-right">
                                    {{ Request::get('pax') }}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Harga/Pax
                                </td>
                                <td class="text-right">
                                    <sup>Rp</sup> {{ number_format($model->harga, 0, ',','.') }}
                                </td>
                            </tr>
                            <tr class="total">
                                <td>
                                    Total
                                </td>
                                <td class="text-right">
                                    <sup>Rp</sup> {{ number_format($model->harga * Request::get('pax'), 0, ',','.') }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <button type="submit" class="btn_full">Book now</button>
                </div>
            </aside>

        </div><!--End row -->
    </div>
</form>
@endsection