@extends('layouts.frontend.main')

@section('content')
<div class="container margin_60" style="margin-top:30px">

    <div class="row">
        <aside class="col-lg-3 col-md-3">

            <div class="search_bar" style="padding:0; width:100%; margin-bottom:15px">
                <form action="{{ route('search', ['w' => Request::get('w'), 's' => Request::get('s')]) }}" method="GET">
                    <div class="nav-searchfield-outer">
                        <input type="text" style="padding:3px 40px 0 10px;" autocomplete="off" name="q" placeholder="Cari tour..."  id="twotabsearchtextbox" value="{{ Request::get('q') }}">
                    </div>
                    <div class="nav-submit-button">
                        <input type="submit" class="nav-submit-input" value="">
                    </div>
                </form>
            </div><!-- End search bar-->

            <div class="box_style_cat">
                <ul id="cat_nav">
                    <li>
                        <a href="{{ route('search', ['w' => 0, 's' => Request::get('s'), 'q' => Request::get('q')]) }}" id="{{ Request::get('w') == 0 ? 'active' : null}}">
                            <i class="icon_set_1_icon-51"></i>Semua Wilayah
                        </a>
                    </li>

                    @foreach($wilayahs as $wilayah)
                    <li>
                        <a href="{{ route('search', ['w' => $wilayah->id, 's' => Request::get('s'), 'q' => Request::get('q') ]) }}"  id="{{ Request::get('w') == $wilayah->id ? 'active' : null}}">
                            <i class="icon_set_1_icon-39"></i>{{$wilayah->nama }}
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div>
        </aside><!--End aside -->
        <div class="col-lg-9 col-md-9">

            <div id="tools">

                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-6">
                        <div class="styled-select-filters">
                            <form action="{{ route('search', ['w' => Request::get('w')]) }}" method="GET">
                                <select name="s" id="sort">
                                    <option value="0">Terpopuler</option>
                                    <option value="1">Nama [A-Z]</option>
                                    <option value="2">Nama [Z-A]</option>
                                    <option value="3">Harga terendah</option>
                                    <option value="4">Harga tertinggi</option>
                                </select>
                            </form>
                        </div>
                    </div>

                </div>
            </div><!--/tools -->

            @foreach($models as $model)
            <div class="strip_all_tour_list">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="img_list">
                            <a href="{{ route('tour', ['id' => $model->id])}}">
                                <img src="{{ $model->getImagePath($model->getImage()) }}" alt="Image">
                                <div class="short_info">
                                    <i class="icon_set_1_icon-41"></i>{{ $model->getWilayah->nama }} 
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix visible-xs-block"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="tour_list_desc">
                            <h3>{{ $model->nama }}</h3>
                            <p> {{ short_description($model->deskripsi, 20) }} </p>
                            
                            <div class="rating" style="margin-bottom:30px">
                                <i class="icon_set_1_icon-29"></i> {{ $model->owner->nama_usaha }}
                            </div>
                            <div style="color: #e74c3c; font-size:24px">
                                <sup>Rp</sup> {{ number_format($model->harga, 0, ',','.') }}
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <div class="price_list">
                            <div>
                                <p><a href="{{ route('tour', ['id' => $model->id]) }}" class="btn_1">Details</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--End strip -->
            @endforeach

            <hr>

            <div class="text-center">
                {!! $models->appends(['w' => Request::get('w'), 's' => Request::get('s'), 'q' => Request::get('q')])->links() !!}
            </div><!-- end pagination-->

        </div><!-- End col lg-9 -->
    </div><!-- End row -->
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function(){
        var s = {{ Request::get('s', 0) }};
        var url = "{!! route('search', ['w' => Request::get('w'), 'q' => Request::get('q')]) !!}";

        $('#sort').val(s);
        $('#sort').on('change', function(){
            window.location.href = url + '&s='+$(this).val();
        });
    });
</script>
@endpush