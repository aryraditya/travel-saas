<?php

/*
  |--------------------------------------------------------------------------
  | ADMIN Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */
Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {
    Route::get('login', 'LoginController@showLoginForm')->name('auth.login');
    Route::post('login', 'LoginController@login')->name('auth.login');
    Route::get('logout', 'LoginController@logout')->name('auth.logout');
});

Route::group(['middleware' => 'auth.admin'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');


    Route::resource('layanan', 'LayananController');
    Route::resource('wilayah', 'WilayahController');
    Route::resource('transaksi', 'TransaksiController');
    Route::group(['as' => 'users.', 'namespace' => 'Users', 'prefix' => 'users'], function() {
        Route::resource('admin', 'AdminController' ,[]);
        Route::resource('member', 'MemberController' ,[]);
        Route::resource('customer', 'CustomerController' ,[]);
    });
});

