<?php

/*
  |--------------------------------------------------------------------------
  | ADMIN Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function() {
    Route::get('login', 'LoginController@showLoginForm')->name('auth.login');
    Route::post('login', 'LoginController@login')->name('auth.login');
    
    Route::get('register', 'RegisterController@showForm')->name('auth.register');
    Route::post('register', 'RegisterController@register')->name('auth.register');
    
    Route::get('pembayaran/{id}', 'RegisterController@pembayaran')->name('auth.pembayaran');
    Route::post('pembayaran/{id}', 'RegisterController@konfirmasi')->name('auth.pembayaran');
    
    Route::get('logout', 'LoginController@logout')->name('auth.logout');
});

Route::group(['middleware' => 'auth.member'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    
    Route::get('pemesanan/print','PemesananController@prints')->name('pemesanan.print');
    Route::resource('pemesanan','PemesananController');
    Route::resource('tour','TourController');
    Route::resource('bank','BankController');
    
    Route::get('account', 'AccountController@edit')->name('account');
    Route::post('account', 'AccountController@update')->name('account');
    
    Route::get('account/upgrade', 'AccountController@upgrade')->name('upgrade');
    Route::post('account/upgrade', 'AccountController@storeUpgrade')->name('upgrade');
    
    Route::get('account/upgrade/konfirmasi/{id}', 'AccountController@pembayaran')->name('upgrade.konfirmasi');
    Route::post('account/upgrade/konfirmasi/{id}', 'AccountController@konfirmasi')->name('upgrade.konfirmasi');
});

