<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'HomeController@index')->name('home');
Route::get('search', 'TourController@search')->name('search');


Route::get('tour/{id}', 'TourController@detail')->name('tour');
Route::get('pesan', 'TourController@pesan')->name('pesan');
Route::post('pesan', 'TourController@proses')->name('pesan');
Route::get('pesan/bayar/{id}', 'TourController@konfirmasi')->name('bayar');
Route::post('pesan/bayar/{id}', 'TourController@bayar')->name('bayar');

Route::get('profil/pesanan', 'ProfileController@pesanan')->name('mybooking');
Route::get('profile/setting', 'ProfileController@setting')->name('mysetting');
Route::post('profile/setting', 'ProfileController@simpan')->name('save_profile');

Route::auth();
Route::get('logout', 'Auth\LoginController@logout')->name('logout');